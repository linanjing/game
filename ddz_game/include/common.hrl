-ifndef('___COMMON_H__').
-define('___COMMON_H__', true).

-include("tools.hrl").
-include("db.hrl").
-include("macro.hrl").
-include("td.hrl").
-include("log.hrl").
-include("battle_record.hrl").
-include("logic_record.hrl").
-include("error_report.hrl").
-include("pt.hrl").
-include("agent.hrl").
-include_lib("stdlib/include/qlc.hrl").

-endif. %%-ifndef('___COMMON_H__').