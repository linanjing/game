-ifndef('__RECORD_H__').
-define('__RECORD_H__', true).

-record(cache,  {uid,cached = 0}).

-record(map_point,{x=0,y=0,z=0,grid_status=false}).

-record(scene_svr, {idx = 0, pid =0,max_scene=0,scene=0,server_name=""}).
-record(agent_svr, {idx=0,pid =0,max_agent=0}).
-record(scene,  {id,owner=0, type=0,club_id=0,module = 0,pid=0,num=0,scene_idx=0,
							create_time=0,all_time=0,status=0,status_time=0,currecy_type=0}). 


-record(st_error_config,{id=0,error="错误"}).
-record(player,{id=0,sid=0,ai=no,head_id=0,name="",
				old_coin=0,card_info,coin=0,currency=0,select=[],multy_select=[],leave=0}).
-record(card,{col=0,value=0,index=0,open=0}).
-record(club_card_info,{club_card_id=0,rebate=0,club_user_bear=0,platform_bear=0,benefits=0}).


-record(battle_room,{id=0,map_id=0,teamList=[],kda=0}).
-record(chat_config, {id = 0, type = 0}).


-record(ply,{uid =0,sid=0,name="",aid ="",sex=0,head_id=0,room_id=0,roomType=0,head_img="",device_no="",interger=0,
				coin=0,currency=0,club_info_id=0,club_card_id=0,online=1}).    
-record(off_ply,{aid = 0,room_id=0,room_type=0}).

%%room 
-record(room, {id,owner=0,room_hid=0,create_time=0,room_status,req=""}). 
-record(room_qp,{status,game_times = 0,pick_qps,jie_suan_list,dz_card=[],dz_uid=0,next_qiang_dz=0,next_index,next_status,last_time}).
-record(room_last,{last_index=0,last_status =0,last_qp =[]}).
-record(room_ply,{uid,sid,name,aid ="",sex,head_img="",score=0,prepare_state =0,pos=0,offline=0,
                  trusteeship=0,card_info =[]}).

-record(logic_agent,     {pid =0,sid=0,ip=0,id=0,skin=0,uid=0,token_id=0,battle_id=0,name="",headUrl=""}).


-record(match_battle,{id=0,map_id=0,usr_info_list=[],create_time=0}).

-record(shop_config, {id = 0, item_id =0, limit = 0, cost = 0, type = 0}).
-record(sign_config, {day = 0, common_sign = [], luxury_sign = []}).
-record(gift_config, {id = 0, reward = [], limit = 0, type = 0}).
-record(achieve_config, {id = 0, reward = [], limit = 0, type = 0, point = 0}).


-record(st_niuniu_config,{id=2,roomId=2,fapai_time=3000,kaishixiazhutexiao_time=3000,xiazhu_time=15000,tingzhixiazhutexiao_time=3000,
					kapaijiange_time=4000,kaipaitexiao_time=3000,jiesuan_time=6000,zhuangjiakaipai=2500,all_time=30000}).

-record(pt_guild_info,{id=0,name="",my_open_game=[],online_player_num=0,guild_head=0,card=0,
						all_player_num=0,creater_id=0,all_req_enter_list=[],myreq_state=0,integer=0}).
-record(pt_enter_club_info,{id=0,name="",head_id=0,state=0}).
-record(pt_club_member_info,{id=0,name="",head=0,online=0,integral=0,enter_time=0,game_num=0,last_login_time=0}).
-record(pt_table_info,{club_id=0,game_type=0,table_id=0}).
-record(pt_club_card_info,{id=0,club_name="",create_name="",use=0}).
-record(pt_member_card_info,{uid=0,name="",headId=0,is_online=0,interger=0,proportion=0,benifit=0,rebate=0}).
-record(scene_coin,{coin=0,interger=0,currency=0}).

-record(pt_club_game_info,{time=0,scene_type=0,scene_id=0,update=0}).

-define(GAME_STATUS,game_status).
-define(DEASL_TIME,deal_time).
-define(STOP_TIME,stop_time).
-define(CardValueList,[1,2,3,4,5,6,7,8,9,10,11,12,13]).
-define(MachineId,1000000).
-define(SceneCurrency,scene_currency).
-define(Coin,coin).
-define(Currency,currency).
-define(Interger,interger).
-define(CurrencyType,currencyType).
-endif.  %%-ifndef('__RECORD_H__').