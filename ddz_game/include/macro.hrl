-ifndef('__MACRO_H__').
-define('__MACRO_H__', true).


-define(ROOM_WAIT,1). %%房间等待
-define(ROOM_GAME,2). %%房间游戏开始


-define(REQ_PREPARE,1).   %%房间里面点准备

-define(REQ_QIANG_DZ,3).  %%请求抢地主
-define(REQ_NO_QIANG_DZ,4).  %%点击不抢地主
-define(REQ_NO_PLAY_CARD,5).  %%不出牌 
-define(REQ_TRUSTEESHIP,6).  %%点击托管
-define(REQ_CANCEL_TRUSTEESHIP,7).  %%取消托管
-define(REQ_JIAO_DZ,8).  %%叫地主
-define(REQ_NO_JIAO_DZ,9).  %%不叫地主


%%更新状态类型
-define(UPDATE_PREPARE_STATUS,1).  %%通知 准备
-define(UPDATE_MINGPAI_STATUS,2).  %%通知 明牌状态
-define(UPDATE_QIANG_DZ_STATUS,3). %%抢地主状态
-define(UPDATE_PLAYING_CARD_STATUS,4).  %%通知 不出牌同步状态
-define(UPDATE_CAN_QIANG_STATUS,5).     %%通知可以抢地主了

-define(UPDATE_JIAO_DZ_STATUS,7).     %%通知叫地主

-define(STEP_1,1).  %% 准备倒计时阶段
-define(STEP_2,2).  %% 抢地主倒计时阶段 
-define(STEP_3,3).  %% 出牌倒计时阶段

-define(COUNT_DOWN,10). %%10秒倒计时


-define(RET_SUCC, 0).
-define(RET_INVALID_ARG, -1).

-define(MAX_INT, 2147483647).
-define(MIN_INT, -2147483648).
-define(MAX_UINT, 4294967295).

-define(MAX_SERVER_ID, 999).

-define(STATE_ONLINE,1).
-define(STATE_OFFLINE,2).



% resources

% channel




% code
-define(TRUE, true).
-define(FALSE, false).
-define(CONTINUE, continue).
-define(UNDEFINED, undefined).
-define(OK, ok).
-define(SKIP, skip).
-define(RETURN, return).
-define(ERROR, error).

% system
-define(MAX_PLAYER_LEV,166).

-define(DIFF_SECONDS_1970_1900, 2208988800).
-define(DIFF_SECONDS_0000_1900, 62167219200).
-define(ONE_DAY_SECONDS,        86400).
-define(ONE_HOUR_SECONDS,		3600).
-define(ONE_MIN_SECONDS,		60).
-define(LEAVE_FIGHT_TIME,5).

%%金币场
-define(COIN_SCENE,1).

%%虚拟币场
-define(CURRENCY_SCENE,2).


%%服务器时间(秒)
-define(NOW,util:unixtime()). 
%% 服务器实体sort
-define(SPIRIT_SORT_NULL,no).
-define(SPIRIT_SORT_NPC,npc).
-define(SPIRIT_SORT_ITEM,sceneitem).
-define(SPIRIT_SORT_MONSTER,monster).
-define(SPIRIT_SORT_USR,usr).
-define(SPIRIT_SORT_PET,pet).
-define(SPIRIT_SORT_MODEL,model).
-define(SPIRIT_SORT_PICKABLE_ITEM,scene_pickable_item).
-define(SPIRIT_SORT_ROBOT, robot).


%%系统消息发送者
-define(GM1,"GM").
-define(GM2,"系统").
%%邮件类型 
-define(PERSONAL_MAIL,1).
-define(GM_MAIL,2).

%%系统邮件文本(服务端决定标题，客户端决定内容) 
-define(GEM_MAIL,1).




%%消息类型
-define(World,1).
-define(Team,2).
-define(Guild,3).
-define(Private,4).
-define(System,5). 
-define(Special,6). 


-define(UP_OK,1).
-define(UP_DEFEAT,0).
-define(COMMOM_REST_HOUR,0).
-define(GET_OK,1).
-define(NO_GET,0).
-define(PREPARE_OK,1).
-define(CANCLE_OK,2).
-define(THREE_STAR,3).
-define(TWO_STAR,2).
-define(ONE_STAR,1).
-define(WIN,1).
-define(DEFEAT,0).
-define(DRAW,draw).
-define(USR_KILL_ROBOT,1).
-define(ROBOT_KILL_USR,2).
-define(USR_KILL_USR,3).
-define(USR,0).
-define(ROBOT,1).
%%场景type
-define(SCENE_TYPE_ARENA,160001).

%%NPC交互距离
-define(NPC_INTERACT_DIS, 7).









%%每天刷新时间
-define(AUTO_REFRESH_TIME,0).


%%错误报告
-define(GUILD_001,1).





-endif.