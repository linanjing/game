-ifndef('__TD_H__').
-define('__TD_H__', true).
%%,pass_last_ref_time=0


-record(us_club,{id=0,player_num=0,club_name="",game_uid=0,user_id="",create_time=0,club_info_id=0,flow_card=0}).
-record(us_club_member,{id=0,game_uid=0,name="",user_id="",club_info_id=0,player_state=0,enter_time=0,card=0,interger=0}).
-record(us_club_card,{id=0,club_card_id=0,uid=0,rebate=0,club_user_bear=50,platform_bear=50,benefits=50000}).
-record(us_game_record,{id=0,game_uid=0,time=0,rebate=0,bet_currency=0,benifit_add=0}).
-record(us_account,{account_id="",user_id="",amount=0.0,create_time=""}).
-record(us_user_info,{user_id="",user_game_id=0,nick_name="",phone="",password="",salt="",user_status="",user_type="",sex="",head_portrait="",
					  device_no="",update_time="",create_time=""}).
-record(us_game_record_card,{id=0,time=0,name="",bet=0,rebate=0,date="",club_info_id=0}).
-record(us_game_record_benifit,{id=0,time=0,name="",proportion=0,rebate=0,date="",club_info_id=0}).
-record(us_game_record_amount,{id=0,uid=0,time=0,name="",scene_id=0,scene_type=0,amount=0,time_str="",club_info_id=0}). %%积分记录
-record(us_game_record_amount_creater,{id=0,time=0,scene_id=0,scene_type=0,time_str="",end_amount=0,club_info_id=0}). %%积分记录
-record(us_game_interger_update,{id=0,time=0,club_info_id=0,uid=0,interger=0}).
-define(Tabs, 
		[
		  [us_club | record_info(fields, us_club)],
		  [us_club_member | record_info(fields, us_club_member)],
		  [us_club_card | record_info(fields, us_club_card)],
		  [us_account | record_info(fields, us_account)],
		  [us_user_info | record_info(fields, us_user_info)],
		  [us_game_record_card | record_info(fields, us_game_record_card)],
		  [us_game_record_benifit | record_info(fields, us_game_record_benifit)],
		  [us_game_record_amount | record_info(fields, us_game_record_amount)],
		  [us_game_record_amount_creater | record_info(fields, us_game_record_amount_creater)],
		  [us_game_interger_update | record_info(fields, us_game_interger_update)]
			
		]).
   
-define(StartLoadList,
		[

		 ]).
 
-define(PlyLoadList, 
		[
		]).



-define(WorldTabs,
		[
		 ply,
		 scene,
		 us_game_record_card,	
		 us_game_record_benifit,
		 us_game_record_amount,
		 us_game_record_amount_creater,
		 us_club,
		 us_club_member,
		 us_club_card,
		 us_account,
		 us_user_info
		]).


-define(AgentTabs,
		[
		]).

-define(SceneTabs,
		[
		 ]).








-endif. %%-ifndef('__TD_H__').