-module(handler_share_friend).

-export([init/3]).
-export([handle/2]).

-export([info/3]).
-export([terminate/3]).

-include("common.hrl").

init(_, Req, _Opts) ->
%% 	?log("global_wx_handler........init.._Opts =~p",[_Opts]),
	{ok, Req, undefined}.

handle(Req, State) ->
	?log_trace("recv http request,peer=~p,path=~p,body=~p",[cowboy_req:get(peer,Req),cowboy_req:get(path,Req),cowboy_req:get(buffer,Req)]),
	{_Method, Req1} = cowboy_req:method(Req),
	_HasBody = cowboy_req:has_body(Req1),	
	JsStr=rfc4627:encode({obj, [  
								{"code", 0},
								{"heziAppId", 1},
								{"fenxiangDesc",unicode:characters_to_binary("天呐，居然可以钓起这么大的鱼#鱼塘养鱼，天天上线就能领金币#开局一杆钓，什么全靠钓#海王超长中级预告来袭，引爆网络，口碑炸裂!")},
								{"fenxiangUrl",unicode:characters_to_binary("https://fishing.huiruui.com/commonimage/1.jpg#https://fishing.huiruui.com/commonimage/2.jpg#https://fishing.huiruui.com/commonimage/3.jpg#https://fishing.huiruui.com/commonimage/4.jpg")}
							   ]}),
	{ok, NewReq} = cowboy_req:reply(200,  [{<<"content-type">>,<<"text/html; charset=UTF-8">>}],JsStr, Req),
	{ok, NewReq, State}.

%% unicode:characters_to_binary("跟随小锦鲤,开始一段洗涤心灵的旅程！#守护这朵村花,考试第一,升职加薪！#玩锦鲤游戏交好运,下个鸿运当头的就是你！#治愈系浓郁国风,微信最火爆的锦鲤")
%% cowboy_req:reply(200, #{}
%% 		<<"content-type">> => <<"application/json; charset=utf-8">>
%% 	}, JsStr, Req1).


info({reply, Body}, Req, State) ->
	?log("reply.........share_friend_handler.................. "),
    {ok, Req2} = cowboy_req:reply(200, [], Body, Req),
    {ok, Req2, State};
info(_Msg, Req, State) ->
	?debug("unknown msg,~p",[_Msg]),
    {loop, Req, State, hibernate}.

terminate(_Reason, _Req, _State) ->
	?debug("terminate,~p",[_Reason]),
	ok.


