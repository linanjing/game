-module(syslog_sup).
-behaviour(supervisor).

-export([start_link/0]).
-export([init/1]).
-export([start/0]).

%%--------------------------------------------------------------------
start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

start() ->
    ?MODULE:start_link().

init([]) ->
	Host = 
		case application:get_env(host) of
			{ok, Host1} -> Host1;
			_ -> "localhost"
		end,
	TcpPort =
		case application:get_env(port_tcp) of
			{ok, TcpPort1} -> TcpPort1;
			_ -> 5140
		end,
	UdpPort =
		case application:get_env(port_udp) of
			{ok, UdpPort1} -> UdpPort1;
			_ -> 514
		end,
	Factility = 
		case application:get_env(facility) of
			{ok, Facility1} -> Facility1;
			_ -> local1
		end,
    Children = {syslog_server, {syslog_svr, start_link, [{Host, TcpPort, UdpPort, Factility}]}, permanent, 2000, worker, [syslog_svr]},
    RestartStrategy = {one_for_one, 10, 10},
    {ok, {RestartStrategy, [Children]}}.

