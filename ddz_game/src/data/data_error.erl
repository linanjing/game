-module(data_error).
-include("common.hrl").

-export([get_data/1]).
-export([get_all/0]).

get_data(1)->#st_error_config{id=1,error="输入的俱乐部 ID 不存在"};
get_data(2)->#st_error_config{id=2,error="您不是俱乐部创建人,只有俱乐部创建人才能做这个操作"};
get_data(3)->#st_error_config{id=3,error="您已经被请离该俱乐部"};
get_data(_) ->{}.


get_all()->[1,2].
