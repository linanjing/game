-module(scene_sup).
-behaviour(supervisor).

-export([start_link/0]).
-export([init/1]).
-export([start/0]).
-export([add/1]).

%%--------------------------------------------------------------------
start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

start() ->
    ?MODULE:start_link([]).

init([]) ->
    Children = {scene, {scene, start_link, []}, temporary, 2000, worker, [scene]},
    RestartStrategy = {simple_one_for_one, 10, 10},
    {ok, {RestartStrategy, [Children]}}.
   
add(Scene_Data) ->
    supervisor:start_child(?MODULE, [Scene_Data]).

