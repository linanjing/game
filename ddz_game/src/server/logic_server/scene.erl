-module(scene).
-behaviour(gen_server).

-export([start_link/1, stop/0, init/1, terminate/2, code_change/3, handle_cast/2, handle_info/2, handle_call/3]).

-include("common.hrl").

-record(state, {id = 0,type=0,start_time=0,on_time_check=0,on_time_off=0,on_time_module=0,scene_model=0,script_items=[]}).
-define(SCENE_TIMER_INTERVAL,200).


start_link(Data) ->
    gen_server:start_link(?MODULE, Data, []).

stop() ->
    gen_server:cast(?MODULE, stop).  

init({UsrInfoList,SceneId,SceneType}) ->
	put(scene_id,SceneId),
	put(scene_type,SceneType),
	rand:uniform(),
	{Module,InitFunc}=fun_scene_select:get_init_info(SceneType),
	?debug("init scene====Module~p,InitFunc=~p",[Module,InitFunc]),
	erlang:put(module, Module),
	try
			Module:InitFunc(UsrInfoList)
	catch E:R ->
			  ?log_error("E=~p,R=~p,error=~p",[E,R,erlang:get_stacktrace()])
	end,	
	{currencyType,CurrencyType}=lists:keyfind(currencyType, 1, UsrInfoList),
	case lists:keyfind(sid, 1, UsrInfoList) of
		false->
				gen_server:cast({global, agent_mng}, {scene_add,{self(),
																 erlang:get(scene_id),
																 erlang:get(scene_type),0,CurrencyType}});
		{sid,Sid}->
			
			[Club_Info=#pt_table_info{club_id=ClubId}]=
			case lists:keyfind(club_info, 1, UsrInfoList) of
				false->[];
				{club_info,PtInfo}->PtInfo
			end,
			
			gen_server:cast({global, agent_mng}, {scene_add,{self(),erlang:get(scene_id),
															 erlang:get(scene_type),
															 ClubId,CurrencyType}}),
			pt:send(Sid, ?PT_RES_CREATE_TABLE, {1,[Club_Info]})
	end,
	{ok, #state{}}.

    

terminate(_Reason, #state{id =ID}) ->
	?debug("scene terminate,id=~p,scene=~p",[ID,get(scene)]),
	gen_server:cast({global, scene_mng},{scene_del,ID}),		
	ok.

code_change(_OldVsn, State, _Extra) ->    
    {ok, State}.


handle_call({apply, Module, Func, Args}, _From, State) ->
	Ret = 
	try
		erlang:apply(Module, Func, Args)
	catch
		E:R -> {call_failed, {E,R,erlang:get_stacktrace()}}
	end,
	{reply, Ret, State};
handle_call(_Request, _From, State) ->    
    {reply, ok, State}.

handle_info({timeout, TimerRef, {xtimer_callback, CallBackInfo}}, State) ->
	try
		xtimer:on_timer(TimerRef, CallBackInfo)
	catch E:R ->?log_error("timeout error,data=~p,E=~p,R=~p,stack=~p",[CallBackInfo,E,R,erlang:get_stacktrace()])
	end,
	{noreply, State};
%% handle_info({time},  #state{type =Scene,on_time_off=_Off,on_time_module=_Ontime,on_time_check=_CheckTime,scene_model = SceneModel,script_items=_Script_items} = State) -> 	
%% 	Now = util:longunixtime(),
%% 	try
%% 		fun_scene:do_time(Scene,Now)
%% 	catch
%% 		ES:RS -> 
%% 			?log_error("scene doTimer error SceneModel=~p,E=~p,R=~p,c=~p",[SceneModel,ES,RS,erlang:get_stacktrace()])
%% 	end,
		
handle_info(Request, State) ->
	?log_warning("unknown info,info=~p",[Request]),
    {noreply, State}.

handle_cast({load}, #state{type =Scene,scene_model = SceneModel} = State) ->
	
%% 	fun_scene:test(),
%% 	try 
%% 		fun_scene:on_scene_loaded(SceneModel,Scene)
%% 	catch 
%% 		E:R -> ?log_error("scene on_scene_loaded error E=~p,R=~p,stack=~p",[E,R,erlang:get_stacktrace()])
%% 	end,
%% 	erlang:send_after(?SCENE_TIMER_INTERVAL, self(), {'$gen_cast', {time}}),
%%	timer:apply_after(?SCENE_TIMER, gen_server, cast, [self(), {time}]),
%% 	case SceneModel of
%% 		fun_scene_copy -> skip;
%% 		_ -> timer:apply_after(?SCENE_TIMER, gen_server, cast, [self(), {time}])
%% 	end,	
	{noreply, State};




handle_cast({agent_out, Uid}, #state{id =_ID} = State) ->    
    ?log_trace("agent_out, uid = ~p", [Uid]),
	try
		fun_scene_usr:leave_scene(Uid)
	catch
		ES:RS -> 
			?log_error("scene usr_enter_scene error E=~p,R=~p,c=~p",[ES,RS,erlang:get_stacktrace()])
	end,
    {noreply, State};

handle_cast({time},  #state{type =Scene,on_time_off=_Off,on_time_module=_Ontime,on_time_check=_CheckTime,scene_model = SceneModel,script_items=_Script_items} = State) -> 	
	Now = util:longunixtime(),
	try
		fun_scene:do_time(Scene,Now)
	catch
		ES:RS -> 
			?log_error("scene doTimer error SceneModel=~p,E=~p,R=~p,c=~p",[SceneModel,ES,RS,erlang:get_stacktrace()])
	end,
		
	
%% 	NewCheckTime =if
%% 					  Off > 0 -> 
%% 						  if
%% 							  Now - CheckTime > Off -> 
%% 								  try
%% 									  Ontime:on_time(Scene,Now - StartTime)
%% 								  catch
%% 									  E:R ->
%% 										  ?log_error("scene ontime script error Scene=~p,E=~p,R=~p,S=~p",[Scene,E,R,erlang:get_stacktrace()])
%% 								  end,
%% 								  Now;
%% 							  true -> CheckTime
%% 						  end;
%% 					  true -> CheckTime
%% 				  end,
%% 	
	Te = util:longunixtime(),
	if
		Te - Now > 50 -> ?log("scene time over time,time=~p,Scene=~p",[Te - Now,Scene]);
		true -> skip
	end,
	erlang:send_after(?SCENE_TIMER_INTERVAL, self(), {'$gen_cast', {time}}),
	%%timer:apply_after(?SCENE_TIMER, gen_server, cast, [self(), {time}]),	
    {noreply, State#state{on_time_check = Now}};

handle_cast(stop, #state{type =Scene} = State) ->
	try 
		fun_scene:on_close(Scene)
	catch 
		E:R -> ?log_error("scene on_close error E=~p,R=~p,stack=~p",[E,R,erlang:get_stacktrace()])
	end,
    {stop, normal, State};

handle_cast({apply, Module, Func, Args}, State) ->
	try
		erlang:apply(Module, Func, Args)
	catch
		E:R -> ?log_error("apply error E=~p,R=~p,stack=~p", [E,R,erlang:get_stacktrace()])
	end,
	{noreply, State};

handle_cast(Msg, State) ->
	?debug("Msg=~p",[Msg]),
	try 
		fun_scene:do_msg(Msg)
	catch 
		E:R -> ?log_error("scene handle_cast error E=~p,R=~p,stack=~p",[E,R,erlang:get_stacktrace()])
	end,		   
	{noreply, State}.