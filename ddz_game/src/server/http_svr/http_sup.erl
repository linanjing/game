%% @author Administrator
%% @doc @todo Add description to http_sup.


-module(http_sup).
-behaviour(supervisor).
-export([init/1]).
-export([start/0,start_link/0]).


start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

start() ->
    ?MODULE:start_link().


init([]) ->
    AChild = {http_svr,  {http_svr,start_link,[]}, permanent, 5000, worker, [http_svr]},
    {ok,{{one_for_all,10,10}, [AChild]}}.



