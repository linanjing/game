-module(pt_game2c).
-export([write/2]). 
-include("common.hrl").


write(?PT_RES_LOGIN, {Usr,Name,LoginType,SceneIdList})->
	F1=fun(Type)->
			  <<Type:?u32>> end,
	List_Bin=[F1(N)||N<-SceneIdList],
	PRlen=length(List_Bin),
	PBin1=list_to_binary(List_Bin),
	Uid=Usr#ply.uid,
	Coin=Usr#ply.coin,
	HeadId=10,
	Currency=Usr#ply.currency,
	Need=trunc(Currency),
	?debug("Uid=~p,Coin=~p,HeadId=~p,Currency=~p",[Uid,Coin,HeadId,Need]),
	Data= <<Uid:?u32,Coin:?u32,HeadId:?u32, Need:?u32>>,
	Bin1=pt:add_string(Data, Name), 
	Bin= <<Bin1/binary,LoginType:?u32,PRlen:?u16,PBin1/binary>>,
	{?OK, pt:pack(?PT_RES_LOGIN, Bin)};

write(?PT_RES_BET_CARD_INFO, {Ptlist})->
	Bin=pt:pack_rebate_list(Ptlist),
	{?OK, pt:pack(?PT_RES_BET_CARD_INFO, Bin)};
write(?PT_RESOURCE_UPDATE, {Uid,Currency,Coin,ClubId,In})->
	Bin = <<Uid:?u32,Currency:?u32,Coin:?u32,ClubId:?u32,In:?u32>>,
	{?OK, pt:pack(?PT_RES_BET_CARD_INFO, Bin)};
write(?PT_RES_BET_BENIFIT_INFO, {Ptlist})->
	Bin=pt:pack_benifit_list(Ptlist),
	{?OK, pt:pack(?PT_RES_BET_BENIFIT_INFO, Bin)};
write(?PT_NOTYFY_NEXT_OPEN_CARD, {Uid,Key,Name})->
	Bin1= <<Uid:?u32,Key:?u32>>,  
	Bin2=pt:add_string(Bin1, Name),
	{?OK, pt:pack(?PT_NOTYFY_NEXT_OPEN_CARD, Bin2)};
%% @doc  俱乐部列表
write(?PT_RES_GUILD_LIST, {GuildInfoList})->	
	Data=pt:pack_guild_info(GuildInfoList),
	{?OK,pt:pack(?PT_RES_GUILD_LIST, Data)};
write(?PT_LEADER_RES_FREEZE_TO_USER, {Type,LeaderId,Name,ClubId})->	
	B1= <<Type:?u32,LeaderId:?u32>>,
	B2=pt:add_string(B1, Name),
	Data= <<B2/binary,ClubId:?u32>>,
	{?OK,pt:pack(?PT_LEADER_RES_FREEZE_TO_USER, Data)};

write(?PT_LEADER_RES_VERIFY_USER, {ClubId,Uid,State})->	
	Data= <<ClubId:?u32,Uid:?u32,State:?u32>>,
	{?OK,pt:pack(?PT_LEADER_RES_VERIFY_USER, Data)};
write(?PT_LEADER_RES_FREEZE_USER, {ClubId,Uid,State})->	
	Data= <<ClubId:?u32,Uid:?u32,State:?u32>>,
	{?OK,pt:pack(?PT_LEADER_RES_FREEZE_USER, Data)};
write(?PT_LEADER_RES_VERIFY_TO_USER, {ClubId,Uid,Info})->	
	Bin=pt:pack_guild_info(Info),
	Data= <<ClubId:?u32,Uid:?u32,Bin/binary>>,
	{?OK,pt:pack(?PT_LEADER_RES_VERIFY_TO_USER, Data)};




write(?PT_RES_ASK_ClUB_INFO, {List})->	
	Data=pt:pack_ask_list(List),
	{?OK,pt:pack(?PT_RES_ASK_ClUB_INFO, Data)};

 write(?PT_LEADER_RES_ASKJOIN_LIST, {ClubId,All,Oline,List,Type})->	
	Bin=pt:pack_club_member_info(List),
	?debug("Bin=~p",[Bin]),
	Data= <<ClubId:?u32,Type:?u32,All:?u32,Oline:?u32,Bin/binary>>,
	{?OK,pt:pack(?PT_LEADER_RES_ASKJOIN_LIST, Data)};
%% @doc  俱乐部列表
write(?PT_RES_SEARCH_GUILD, {State,GuildInfoList})->	
	Data=pt:pack_guild_info(GuildInfoList),
	Bin= <<State:?u32,Data/binary>>,
	{?OK,pt:pack(?PT_RES_SEARCH_GUILD, Bin)};

write(?PT_RES_CHOOSE_CLUB_CARD_INFO, {State})->	
	Bin= <<State:?u32>>,
	{?OK,pt:pack(?PT_RES_CHOOSE_CLUB_CARD_INFO, Bin)};

write(?PT_RES_CLUB_CARD_INFO, {ClubCardInfoList})->
	F=fun(#pt_club_card_info{id=Id,club_name=Name1,create_name=Name2,use=Use})->
			B1= <<Id:?u32>>,
			B2=pt:add_string(B1,Name1),
			B3=pt:add_string(B2,Name2),
			<<B3/binary,Use:?u32>>
			 end,
	ListBin = [F(Data) || Data <- ClubCardInfoList],	
	Rlen = length(ListBin),
    RB = list_to_binary(ListBin),
    Bin= <<Rlen:?u16, RB/binary>>,
	{?OK,pt:pack(?PT_RES_CLUB_CARD_INFO, Bin)};
%% @doc 创建俱乐部返回
write(?PT_RES_CREATE_GUILD, {Ret,Reason,GuildInfoList})->
	Bin1=pt:pack_guild_info(GuildInfoList),
	Bin3 =pt:add_string(<<Ret:?u32>>, Reason),
	Bin2 = <<Bin3/binary,Bin1/binary>>,
	{?OK,pt:pack(?PT_RES_CREATE_GUILD, Bin2)};

write(?PT_RES_JOIN_GAME, {PlayerInfo,Num,GameStatus})->
	F=fun({Id,Coin,HeadId,Name})->
			    Data= <<Id:?u32,Coin:?u32,HeadId:?u32>>,
				pt:add_string(Data, Name)	
			end,
	List_Bin=[F(N)||N<-PlayerInfo],
	PRlen1=length(List_Bin),
	PBin1=list_to_binary(List_Bin),
	Data= <<PRlen1:?u16,PBin1/binary,Num:?u32,GameStatus:?u32>>,
	{?OK,pt:pack(?PT_RES_JOIN_GAME, Data)};

write(?PT_RES_SEARCH_GUILD, {RoomInfoBin})->
	{?OK,pt:pack(?PT_RES_SEARCH_GUILD, RoomInfoBin)};
write(?PT_RES_NIUNIU_ROOM_INFO, {RoomInfoBin})->
	{?OK,pt:pack(?PT_RES_NIUNIU_ROOM_INFO, RoomInfoBin)};
write(?PT_RES_NIUNIU_CARD_INFO, {Uid,Key,CardIndfo,Open_type})->
	Bin=pt:pack_card_list(CardIndfo),
	Ptbin = <<Uid:?u32,Key:?u32,Open_type:?u32,Bin/binary>>,
	{?OK,pt:pack(?PT_RES_NIUNIU_CARD_INFO, Ptbin)};
write(?PT_RES_ROOM_LIST,{SceneList})->
	%%返回不同类型的游戏的桌子列表
	?debug("SceneList=~p",[SceneList]),
	F1=fun({Type,CurrenCy})->
			  <<Type:?u32,CurrenCy:?u32>> end,
	List_Bin=[F1(N)||N<-SceneList],
	?debug("List_Bin=~p",[List_Bin]),
	PRlen=length(List_Bin),
	Bin=list_to_binary(List_Bin),
	Data= <<PRlen:?u16,Bin/binary>>,
	{?OK,pt:pack(?PT_RES_ROOM_LIST, Data)};
write(?PT_NOTYFY_MAX_BET, {Uid,Type,Bet})->
	Ptbin = <<Uid:?u32,Type:?u32,Bet:?u32>>,
	{?OK,pt:pack(?PT_NOTYFY_MAX_BET, Ptbin)};
write(?PT_RES_LEAVE_ROOM,{PlayerNum})->
		Ptbin = <<PlayerNum:?u32>>,
	 {?OK,pt:pack(?PT_RES_LEAVE_ROOM, Ptbin)};
write(?PT_RES_DROP_ALL_BET, {Vic,Bet})->
	Ptbin = <<Vic:?u32,Bet:?u32>>,
	{?OK,pt:pack(?PT_RES_DROP_ALL_BET, Ptbin)};
write(?PT_RES_NIUNIU_BETS, {Id,Type,Bet,Coin,Multiple,BET1,BET2,BET3,BET4})->
%% 			?debug("========~p",[{Id,Type,Bet,Coin,Multiple}]),
			Bin=  <<Id:?u32,Type:?u32,Bet:?u32,Coin:?u32,Multiple:?u32,BET1:?u32,BET2:?u32,BET3:?u32,BET4:?u32>> ,
	{?OK,pt:pack(?PT_RES_NIUNIU_BETS, Bin)};

write(?PT_RES_GAME_BETS, {BetList})->
	?debug("BetList=~p",[BetList]),
	F1=fun({Id,Type,Bet,Coin,X,Y})->
			  	<<Id:?u32,Type:?u32,Bet:?u32,Coin:?u32,X:?u32,Y:?u32>>
				end,
	List_Bin=[F1(N)||N<-BetList],
	PRlen1=erlang:length(List_Bin),	
	PBin1=list_to_binary(List_Bin),
	Bin= <<PRlen1:?u16,PBin1/binary>>,
	{?OK,pt:pack(?PT_RES_GAME_BETS, Bin)};
write(?PT_RES_CREATE_TABLE, {Ret,TableList})->
	?debug("TableList=~p",[TableList]),
	F=fun(#pt_table_info{club_id=ClubId,game_type=Type,table_id=TableId})->
			  	<<ClubId:?u32,Type:?u32,TableId:?u32>> end,
	List_Bin=[F(N)||N<-TableList],
	PRlen1=erlang:length(List_Bin),	
	PBin1=list_to_binary(List_Bin),
	Bin= <<Ret:?u32,PRlen1:?u16,PBin1/binary>>,
	{?OK,pt:pack(?PT_RES_CREATE_TABLE, Bin)};
write(?PT_RES_TABLE_LIST, {TableList})->
	F=fun(#pt_table_info{club_id=ClubId,game_type=Type,table_id=TableId})->
			  	<<ClubId:?u32,Type:?u32,TableId:?u32>> end,
	List_Bin=[F(N)||N<-TableList],
	PRlen1=erlang:length(List_Bin),	
	PBin1=list_to_binary(List_Bin),
	Bin= <<PRlen1:?u16,PBin1/binary>>,
	{?OK,pt:pack(?PT_RES_TABLE_LIST, Bin)};
write(?PT_RES_NIUNIU_GAME_OVER,{VueEndInfo,MasterBet,NewCoin,Vic,VicNum})->
	ValueEndInfo=pt:pack_end_card_info(VueEndInfo),
	Data= <<ValueEndInfo/binary,MasterBet:?u32,NewCoin:?u32,Vic:?u32,VicNum:?i32>>,
	{?OK,pt:pack(?PT_RES_NIUNIU_GAME_OVER, Data)};
write(?PT_RES_GAME_STATUS, {GameStatus,Rest_Time})->
	Data= <<GameStatus:?u32,Rest_Time:?u32>>,
	{?OK,pt:pack(?PT_RES_GAME_STATUS, Data)};

write(?PT_MY_COIN_UPDATE, {Uid,Currency,Coin})->
	Data= <<Uid:?u32,Currency:?u32,Coin:?u32>>,
	{?OK,pt:pack(?PT_MY_COIN_UPDATE, Data)};

write(?PT_RES_GAME_OVER_INFO, {CardList,PlayerInfo})->
	F1=fun({Col,Value})->
			  <<Col:?u32,Value:?u32>> end,
	List_Bin=[F1(N)||N<-CardList],
	PRlen1=length(List_Bin),
	PBin1=list_to_binary(List_Bin),
	F2=fun({Uid,Bets})->
			  <<Uid:?u32,Bets:?u32>> end,
	List_Bin1=[F2(N)||N<-PlayerInfo],
	PRlen2=length(List_Bin1),
	PBin2=list_to_binary(List_Bin1),
	Data= <<PRlen1:?u16,PBin1/binary,PRlen2:?u16,PBin2/binary>>,
	{?OK,pt:pack(?PT_RES_GAME_OVER_INFO, Data)};
write(?PT_ERROR_REPORT, {ERROR})->
	Data= pt:add_string(<<>>, ERROR),
	{?OK,pt:pack(?PT_ERROR_REPORT, Data)};
write(?PT_RES_CLUB_CHANGE_PER, {Club_Info_Id,Uid,ClubCardConfigId})->
	Data= <<Club_Info_Id:?u32,Uid:?u32,ClubCardConfigId:?u32>>,
	{?OK,pt:pack(?PT_RES_CLUB_CHANGE_PER, Data)};
write(?PT_RES_CLUB_USER_CARD_INFO, {PtList})->
	?debug("PtList=~p",[PtList]),
	F=fun(#pt_member_card_info{uid=Uid,name=Name,
							   headId=Head,
							   is_online=Online,
							   interger=Interger,
							   proportion=Club_user_bear,
							   benifit=Benifit,
							   rebate=Rebate})-> 
			B1= <<Uid:?u32>>,
			B2=pt:add_string(B1, Name),
			?debug("B2=~p,Head=~p,Online=~p,Interger=~p,Club_user_bear=~p,Benifit=~p,
					Rebate=~p",[B2,Head,Online,
					Interger,Club_user_bear,Benifit,Rebate]),
			<<B2/binary,Head:?u32,Online:?u32,Interger:?u32,
					Club_user_bear:?u32,Benifit:?u32,Rebate:?u32>> end,
			List_Bin=[F(N)||N<-PtList],
			PRlen1=length(List_Bin),
			PBin1=list_to_binary(List_Bin),
			Data= <<PRlen1:?u16,PBin1/binary>>,
	{?OK,pt:pack(?PT_RES_CLUB_USER_CARD_INFO, Data)};

write(?PT_RES_LEADER_INTERGER_INFO, {PtList})->
	PtBin=pt:pack_inter_record(PtList),
  	{?OK,pt:pack(?PT_RES_LEADER_INTERGER_INFO, PtBin)};

write(?PT_LEADER_RES_UPDATE_INTERGER, {ClubInfoId,Uid,Interger,End})->
	Data = <<ClubInfoId:?u32,Uid:?u32,Interger:?u32,End:?u32>>,
	{?OK,pt:pack(?PT_LEADER_RES_UPDATE_INTERGER, Data)};

write(?PT_RES_UPDATE_INTER, {ClubInfoId,Interger,End})->
	Data = <<ClubInfoId:?u32,Interger:?u32,End:?u32>>,
	{?OK,pt:pack(?PT_RES_UPDATE_INTER, Data)};
write(?PT_RES_INTERGER_RECORD, {List})->
	Data=pt:pack_inter_update(List),
	{?OK,pt:pack(?PT_RES_INTERGER_RECORD, Data)};
write(PT,DATA) ->
	?log_error("~p:no match PT=~p,DATA=~p",[?MODULE,PT,DATA]).
