%% @author Administrator
%% @doc @todo Add description to router.


-module(router).
-include("common.hrl").

-export([handle/5]).



handle(Cmd,Rest,ScenePid,Uid,Sid)-> 
	case pt_c2game:read(Cmd, Rest) of
		{ok,agent_mng,Module,Fun,Data}->
			gen_server:cast({global,agent_mng}, {Module,Fun,{Data,Uid,Sid}});
		{ok,scene,Fun,Data}->
			gen_server:cast(ScenePid, {pt,Fun,{Data,Uid,Sid}});
		_R->
			?log("erorr,_R=~p,",[_R])
	end.