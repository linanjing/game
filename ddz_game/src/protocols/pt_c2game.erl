-module(pt_c2game).
-export([read/2]).
-include("common.hrl").
%% pt:read_list(Bin, {?rtype_tuple, [?rtype_int,?rtype_int]}),

read(?PT_REQ_LOGIN,<<Bin/binary>>)->	
	{Accont,R1} = pt:read_string(Bin),
	{Machine_cord,R2}=pt:read_string(R1),
	{PassWord,R3}=pt:read_string(R2),
	{LoginType,R4}=pt:read_int(R3), 
	{Token,R5}=pt:read_string(R4), 
	{Name,_}=pt:read_string(R5),
	{ok,agent_mng,fun_login,login,{Accont,Machine_cord,PassWord,LoginType,Token,Name}};
%%请求加入房间
read(?PT_REQ_JOIN_GAME,<<Bin/binary>>)->	
	{SceneId,_} = pt:read_int(Bin),
	{ok,agent_mng,fun_login,join,{SceneId}};
read(?PT_REQ_OPEN_CARD,<<Bin/binary>>)-> 
		{Type,Rest} = pt:read_int(Bin),
	    {Value,R1} = pt:read_int(Rest),
		{Open_Type,_}=pt:read_int(R1),
	   {ok,scene,req_open_card, {Type,Value,Open_Type} };
read(?PT_REQ_NIUNIU_BET,<<Bin/binary>>)->	

	{Bets,R1} = pt:read_int(Bin),
	{Type,R2} = pt:read_int(R1),
	{Multy,_} = pt:read_int(R2),
	{ok,scene,bet,{Bets,Type,Multy}};

read(?PT_REQ_MY_ASKJOIN_LIST,<<_Bin/binary>>)->	
	{ok,agent_mng,fun_club,req_ask_join_list,{}};

read(?PT_LEADER_REQ_ASKJOIN_LIST,<<Bin/binary>>)->	
	{ClubId,R1} = pt:read_int(Bin),
	{Page,R2} = pt:read_int(R1),
	{Type,_} = pt:read_int(R2),
	{ok,agent_mng,fun_club,leader_req_ask_join_list,{ClubId,Page,Type}};

read(?PT_REQ_DROP_ALL_BET,<<_Bin/binary>>)->	
	{ok,scene,drop_all_bet,{}};
read(?PT_REQ_CREATE_GUILD,<<Bin/binary>>)->	
	{GuildName,_} = pt:read_string(Bin),
	{ok,agent_mng,fun_club,create_club,{GuildName}};
read(?PT_REQ_GUILD_LIST,<<_Bin/binary>>)->	
	{ok,agent_mng,fun_club,search_club,{}};
read(?PT_REQ_SEARCH_GUILD,<<Bin/binary>>)->	
	{Id,_} = pt:read_int(Bin),
	{ok,agent_mng,fun_club,req_search_club_info,{Id}};
read(?PT_REQ_JOIN_GUILD,<<Bin/binary>>)->	
	{Id,_} = pt:read_int(Bin),
	{ok,agent_mng,fun_club,req_enter_club,{Id}};

read(?PT_REQ_CREATE_TABLE,<<Bin/binary>>)->	
	{ClubInfoId,R1} = pt:read_int(Bin),
	{GameType,_} = pt:read_int(R1),
	{ok,agent_mng,fun_club,req_create_table,{ClubInfoId,GameType}};

read(?PT_REQ_TABLE_LIST,<<Bin/binary>>)->	
	{ClubInfoId,_} = pt:read_int(Bin),
	{ok,agent_mng,fun_club,req_table_list,{ClubInfoId}};

read(?PT_LEADER_REQ_VERIFY_USER,<<Bin/binary>>)->	
	{ClubId,R1} = pt:read_int(Bin),
	{Uid,R2} = pt:read_int(R1),
	{State,_} = pt:read_int(R2),
	{ok,agent_mng,fun_club,verify_usr,{ClubId,Uid,State}};

read(?PT_LEADER_REQ_FREEZE_USER,<<Bin/binary>>)->	
	{ClubId,R1} = pt:read_int(Bin),
	{Uid,R2} = pt:read_int(R1),
	{State,_} = pt:read_int(R2),
	{ok,agent_mng,fun_club,leader_oprate_usr,{ClubId,Uid,State}};

read(?PT_REQ_LEAVE_ROOM,<<_Bin/binary>>)->	
	{ok,scene,leave_room,{}};
read(?PT_REQ_ROOM_LIST,<<Bin/binary>>)->	
	{Type,_} = pt:read_int(Bin),
	{ok,agent_mng,fun_agent,req_room_list,{Type}};


read(?PT_REQ_BET,<<Bin/binary>>)->	
	{Bets,R1} = pt:read_int(Bin),
	{Type,R2} = pt:read_int(R1),
	{X,R3} = pt:read_int(R2),
	{Y,_} = pt:read_int(R3),
	{ok,scene,bets,{Bets,Type,X,Y}};


read(?PT_REQ_CLUB_CARD_INFO,<<_Bin/binary>>)->	

	{ok,agent_mng,fun_club,get_club_card,{}};

read(?PT_REQ_CHOOSE_CLUB_CARD_INFO,<<Bin/binary>>)->	
	{CardId,_} = pt:read_int(Bin),
	{ok,agent_mng,fun_club,choose_card,{CardId}};

read(?PT_REQ_CLUB_CHANGE_PER,<<Bin/binary>>)->
	{ClubInfoId,B1} = pt:read_int(Bin),
	{Uid,B2} = pt:read_int(B1),
	{CardConfigId,_} = pt:read_int(B2),
	{ok,agent_mng,fun_club_card,oprate_card,{ClubInfoId,Uid,CardConfigId}};
read(?PT_REQ_CLUB_USER_CARD_INFO,<<Bin/binary>>)->
	{ClubInfoId,B1} = pt:read_int(Bin),
	{Page,_} = pt:read_int(B1),
	{ok,agent_mng,fun_club_card,get_club_usr_card_info,{ClubInfoId,Page}};

read(?PT_REQ_BET_CARD_INFO,<<Bin/binary>>)->
	{ClubInfoId,B1} = pt:read_int(Bin),
	{Page,_} = pt:read_int(B1),
	{ok,agent_mng,fun_club_card,get_club_usr_card_rebate,{ClubInfoId,Page}};

read(?PT_REQ_BET_BENIFIT_INFO,<<Bin/binary>>)->
	{ClubInfoId,B1} = pt:read_int(Bin),
	{Page,_} = pt:read_int(B1),
	{ok,agent_mng,fun_club_card,get_club_usr_card_benifit,{ClubInfoId,Page}};

read(?PT_LEADER_REQ_UPDATE_INTERGER,<<Bin/binary>>)->
	{ClubInfoId,B1} = pt:read_int(Bin),
	{Uid,B2} = pt:read_int(B1),
	{Type,B3} = pt:read_int(B2),
	{Interger,_} = pt:read_int(B3),
	{ok,agent_mng,fun_club_interger,add_integer,{Type,Uid,Interger,ClubInfoId}};
read(?PT_REQ_LEADER_INTERGER_INFO,<<Bin/binary>>)->
	{ClubInfoId,B1} = pt:read_int(Bin),
	{Page,_} = pt:read_int(B1),
	{ok,agent_mng,fun_club_interger,get_interger_info,{ClubInfoId,Page}};
read(?PT_REQ_INTERGER_RECORD,<<Bin/binary>>)->
	{ClubInfoId,B1} = pt:read_int(Bin),
	{Uid,_} = pt:read_int(B1),
	{ok,agent_mng,fun_club_interger,get_interger_update,{ClubInfoId,Uid}};
read(_Cmd, _R) ->
	?log_error("pt_c2game no_match _Cmd:~p   _R:~p",[_Cmd,_R]),
    {error, no_match}.

