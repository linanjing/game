%% @author Administrator
%% @doc @todo Add description to fun_scene_select.


-module(fun_scene_select).
-include("common.hrl").
-define(BASE_SCENE_ID,200001).
-export([get_init_info/1,get_scene_id/0,get_scene_info/0]).



get_init_info(Id)->
	Module=get_module(Id),
	Func=get_init_func(Module),
	{Module,Func}.




get_module(1)->
	fun_scene_lh;

get_module(2)->
	fun_scene_nn.

get_init_func(fun_scene_lh)->
	init_lh;
get_init_func(fun_scene_nn)->
	init.


get_scene_info()->[1,2].
%% 		List=ets:tab2list(scene),
%% 		F=fun(#scene{id=Id,type=Type})->
%% 				  {Id,Type} end,
%% 		lists:map(F, List).
		
get_scene_id()->
	case erlang:get(base_scene_id) of
		?UNDEFINED->
			erlang:put(base_scene_id, ?BASE_SCENE_ID+1),
			?BASE_SCENE_ID;
		Id-> erlang:put(base_scene_id, Id+1),
			Id+1
	end.
		