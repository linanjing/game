-module(fun_agent).
-include("common.hrl").
%% ====================================================================
%% API functions
%% ====================================================================
-export([on_msg/1,req_room_list/1,update_resource/6]).



%% ====================================================================
%% Internal functions
%% ====================================================================
req_room_list({{Type},_Uid,Sid})->
		List=db:dirty_match(scene, #scene{_='_',type=Type}),
 		F=fun(#scene{id=Id,type=T,currecy_type=Currecy_type},L)->
 				  if
					  Type==T ->L++[{Id,Currecy_type}];
					  true->L
				  end end,
		NeedList=lists:foldl(F, [], List),
		?debug("NeedList=~p",[NeedList]),
		pt:send(Sid, ?PT_RES_ROOM_LIST, {NeedList}).		  
on_msg({pt,Uid,Sid,Data})->
	?debug("pt"),
	case pt_logic:read_short(Data) of
		{9001,_}->server_tools:send_to_agent_mng({refresh});
		_->
			case pt_logic:upack_agent(Data) of
				{ok,DataRecord}->
					pt_routing:routing({Uid,Sid},DataRecord);
				_->skip
			end
	end;

on_msg(gateway_tcp_closed)->
	?debug("tcp_closed"),
%% 	Uid=erlang:get(uid),
%% 	server_tools:send_to_agent_mng({agent_out, Uid}),
	gen_server:cast(self(), stop);

on_msg(Msg)-> 
	?debug("Msg=~p",[Msg]).
update_resource(Uid,Sid,Currency,Coin,ClubId,In)->
	pt:send(Sid, ?PT_RESOURCE_UPDATE, {Uid,Currency,Coin,ClubId,In}).


