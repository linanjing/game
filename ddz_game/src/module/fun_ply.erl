%% @author Administrator
%% @doc @todo Add description to fun_ply.


-module(fun_ply).
-include("common.hrl").
%% ====================================================================
%% API functions
%% ====================================================================
-export([update_currency/2]).



%% ====================================================================
%% Internal functions
%% ====================================================================


update_currency(Uid,Num)->
	case db:dirty_get(ply, Uid) of
		[Ply=#ply{currency=Old}]->
			db:dirty_put(Ply#ply{currency=Old+Num});
		_->skip
	end.