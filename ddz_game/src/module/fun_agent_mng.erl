

-module(fun_agent_mng).
-include("common.hrl"). 


-export([do_msg/1,do_init/0,do_call/1,do_info/1,do_time/1, do_close/0]).



-define(BaseSceneId,100000).
do_time(_Now)->
	5000.

do_init()->
	fun_login:init(),
	fun_club:init(),
	fun_club_card:init(),
	fun_platform_admin:init(),
	erlang:put(idx, 1),
	ets:new(scene_svr, [set,public,named_table,{keypos, #scene_svr.idx}]),
	ets:new(agent_svr, [set,public,named_table,{keypos, #agent_svr.idx}]),
	erlang:send_after(1000, self(), {'$gen_cast', {init_scene}}),
	?debug("fun_agent_mng init").
 

do_msg({init_scene})->
%% 	fun_scene_mng:scene_add([{?SceneCurrency,?Coin},{?CurrencyType,1}],1,1),
%% 	fun_scene_mng:scene_add([{?SceneCurrency,?Coin},{?CurrencyType,1}],1,2),
%% 	fun_scene_mng:scene_add([{?SceneCurrency,?Currency},{?CurrencyType,2}],2,1),
	fun_scene_mng:scene_add([{?SceneCurrency,?Currency},{?CurrencyType,2}],2,2),
	ok;
	

do_msg({scene_add,{ScenePid,SceneId,SceneType,ClubId,CurrencyType}})->
	?debug("scene_add now,ScenePid,SceneId,SceneType,ClubId=~p",[{ScenePid,SceneId,SceneType,ClubId}]),
	db:dirty_put(#scene{id=SceneId,pid=ScenePid,type=SceneType,club_id=ClubId,currecy_type=CurrencyType});

do_msg({update_club_leader,CardId,Bet})->
	?debug("update_club_leader========="),
	case db:dirty_get(us_club, CardId, #us_club.flow_card) of
		[#us_club{game_uid=LeaderId}]->
			?debug("update_club_leader========="),
			case db:dirty_get(ply, LeaderId) of
				[Ply=#ply{currency=Old}]->
					?debug("update_club_leader========="),
					db:dirty_put(Ply#ply{currency=Old+Bet}),
					update_play_coin_or_currency(LeaderId);
				_->?debug("update_club_leader========="),skip
			end;
		_->?debug("update_club_leader========="),skip
	end;
do_msg({agent_out,Uid})->
	case db:dirty_get(ply, Uid) of
		[Ply=#ply{}]->
			?debug("Ply agen_out Uid=~p",[Uid]),
 			db:dirty_put(Ply#ply{online=0,sid=0});
		_->skip
	end;
do_msg({update_club_leader_benefits,ClubId,Benifit})->
	case db:dirty_get(us_club, ClubId, #us_club.club_info_id) of
		[#us_club{game_uid=LeaderId}]->
			?debug("update_club_leader========="),
			case db:dirty_get(ply, LeaderId) of
				[Ply=#ply{currency=Old}]->
					?debug("update_club_leader========="),
					db:dirty_put(Ply#ply{currency=Old+Benifit}),
					update_play_coin_or_currency(LeaderId);
				_->?debug("update_club_leader========="),skip
			end;
		_->?debug("update_club_leader========="),skip
	end;
do_msg({check_acc,Acc,Token})->	
	case gen_server:call({global,http_svr}, {check_acc,Acc,Token},2000) of
		ok->
			skip;
		_->skip
	end;

do_msg({agent_ctr_add, AgentCtrPid, MaxAgent})->
	?debug("agent_ctr_add init"),
	{ok,Idx}=application:get_env(index),
	erlang:put(agent_svr_index, Idx),
	ets:insert(agent_svr, #agent_svr{idx=Idx,pid=AgentCtrPid,max_agent=MaxAgent}),
	N=ets:lookup(agent_svr, Idx),
	?debug("N=~p~n",[N]);
do_msg({scene_ctr_add, SceneCtrPid, MaxScene})->
	?debug("agent_ctr_add init"),
	{ok,Idx}=application:get_env(index),
	erlang:put(scene_svr_index, Idx),
	ets:insert(scene_svr, #scene_svr{idx=Idx,pid=SceneCtrPid,max_scene=MaxScene});
do_msg({create_scene,Scene_Data})->
	Idx=erlang:get(scene_svr_index),
	case ets:lookup(scene_svr, Idx) of
		[#scene_svr{pid=Pid}]->
			gen_server:cast(Pid, {create_scene,Scene_Data});
		_->skip
	end;
do_msg({usr_login_online,Usr,Uid})->
	case db:dirty_get(ply, Uid) of
		[]->
			?debug("usr_login_online============="),
			db:dirty_put(Usr),
			R=db:dirty_get(ply, Uid),
			?debug("R=~p",[R]);
		[Old=#ply{sid=OldSid}]->
			?debug("usr_login_online============="),
			?discon(OldSid,"relogin"),
			Sid=Usr#ply.sid,
			db:dirty_put(Old#ply{sid=Sid})
	end;
do_msg({Module,Function,{Data,Uid,Sid}})->
	?log("Module=~p,Function=~p,Data=~p,Sid=~p,Uid=~p",[Module,Function,Data,Sid,Uid]),
	try
      	Module:Function({Data,Uid,Sid})
	catch 
		E:R->?log("E=~p,R=~p,error=~p",[E,R,erlang:get_stacktrace()])
	end;



do_msg({pt,Uid,Sid,DataRecord})->
	?debug("pt............=~p",[DataRecord]),
	pt_routing:routing({Uid,Sid},DataRecord);


%%离开房间
do_msg({leave_room,Uid}) ->
	case db:dirty_get(ply, Uid) of
		[Ply | _] ->
			db:dirty_put(Ply#ply{room_id=0,roomType=0});
		_ -> skip
	end;




do_msg({battle_svr_add,Pid,ServerName,Index,MaxScene})->
	case ets:lookup(scene_svr, Index) of
		[_Old] ->
			?log_trace("battle_svr_add find old index:~p",[Index]),
			{error, duplicate_index};
		_ -> 
			?log_trace("battle_svr_add,index=~p,MaxScene=~p",[Index,MaxScene]),
			ets:insert(scene_svr, #scene_svr{idx=Index,server_name=ServerName, max_scene=MaxScene, scene=0}),
			erlang:monitor(process, Pid),
			ok 
	end; 
do_msg({update_club_leader_currency_by_card,Uid,CardId,Bet,BetRebate})->
	?debug("update_club_leader_currency_by_card=========="),
	
	case db:dirty_get(us_club, CardId, #us_club.flow_card) of
		[#us_club{game_uid=LeaderId,club_info_id=CLubInfoId}]->
			?debug("update_club_leader_currency_by_card=========="),
			fun_club_card:update_rebate_record(Uid, Bet,CLubInfoId, BetRebate),
			?debug("update_club_leader_currency_by_card=========="),
			fun_platform_admin:flowcard_rebate(LeaderId, Bet);
		_->?debug("fail"),skip
	end;
do_msg({update_club_leader_currency_by_benifit,Uid,CardId,UpdateCoin,ClubUpdate})->
	
	case db:dirty_get(us_club, CardId, #us_club.flow_card) of
		[#us_club{club_info_id=CLubInfoId}]->
			fun_club_card:update_benifit_record(Uid, CLubInfoId,UpdateCoin, ClubUpdate);
		_->skip
	end;
do_msg({update_club_leader_interger,Uid,SceneId,_,SceneType,UpdateInterger,End})->
	?debug("update_club_leader_interger"),
	case db:dirty_get(scene, SceneId) of
		[#scene{club_id=CLubInfoId}]->
			case db:dirty_get(ply, Uid) of
				[#ply{name=Name}]->
					fun_club_interger:update_integer(Uid, Name, SceneId, SceneType, UpdateInterger, CLubInfoId);
				_->?debug("update_club_leader_interger"),skip
			end,
			fun_club_interger:update_integer_creater(SceneId, SceneType, CLubInfoId, End);
		_->?debug("update_club_leader_interger"),skip
	end;
do_msg({leave_room_add_currency,Uid,Coin})->
	case db:dirty_get(ply, Uid) of
		[Old=#ply{currency=Old,sid=Sid}]->
			db:dirty_put(Old#ply{currency=Coin}),
			fun_agent:update_resource(Uid, Sid, Old+Coin, 0, 0, 0);
		_->skip
	end;
		
do_msg({leave_room_add_integer,Uid,Coin,SceneId})->	
	case db:dirty_get(ply, Uid) of
		[#ply{sid=Sid}]->
			case db:dirty_get(scene, SceneId) of
				[#scene{club_id=ClubInfoId}]->
					case db:dirty_match(us_club_member, #us_club_member{_='_',game_uid=Uid,club_info_id=ClubInfoId}) of
						[]->skip;
						[Member=#us_club_member{}]->
							db:dirty_put(Member#us_club_member{interger=Coin}),
							fun_agent:update_resource(Uid, Sid, 0, 0, ClubInfoId, Coin)
					end;
				
				_->skip
			end;
		_->skip
	end;
do_msg({leave_room_add_coin,Uid,Coin})->			
		case db:dirty_get(ply, Uid) of
			[Ply=#ply{coin=Old,sid=Sid}]->
				db:dirty_put(Ply#ply{coin=Coin}),
				fun_agent:update_resource(Uid, Sid, 0, Old+Coin, 0, 0);
			_->skip
		end;

do_msg(Msg)->
	?debug("Msg=~p~n",[Msg]). 

do_call({agent_ctr_add, Pid, MaxAgent}) ->
	Index=erlang:get(idx),
	case ets:lookup(agent_svr, Index) of
		[_Old] ->
			?log_trace("agent_ctr_add find old index:~p",[Index]),
			{error, duplicate_index};
		_ -> 
			?log_trace("agent_ctr_addd,index=~p,MaxAgent=~p",[Index,MaxAgent]),
			ets:insert(agent_svr, #agent_svr{pid=Pid, max_agent=MaxAgent}),
			erlang:monitor(process, Pid),
			ok
	end;
do_call(Other) ->?debug("Other=~p",[Other]).

do_info({'DOWN', _MonitorRef, process, Pid, Info}) ->
	?log_warning("process down,pid=~p,info=~p",[Pid,Info]),
	case ets:match_object(scene_svr, #scene_svr{pid=Pid,_ = '_'}) of
		[#scene_svr{idx=Index}] ->
			?log_warning("scene_ctr down,index=~p",[Index]),
			ets:delete(scene_svr, Index);
		_ -> skip
	end;
do_info(_Info) -> ok.




do_close() ->
	rank_config:save_sql(),
	ok.


%%agent
update_play_coin_or_currency(Uid)->
	case db:dirty_get(ply, Uid) of
		[#ply{coin=Coin,currency=Currency,sid=Sid}]->
			?debug("Sid=~p",[Sid]),
			pt:send(Sid, ?PT_MY_COIN_UPDATE, {Uid,Coin,Currency});
		_->skip
	end.