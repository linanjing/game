%% @author Administrator
%% @doc @todo Add description to fun_http_client.


-module(fun_http_client).
-include("common.hrl").
%% ====================================================================
%% API functions
%% ====================================================================
-define(HTTP_HOST,"192.168.0.39").
-define(HTTP_PORT,8080).
-define(HTTP_URL,"/user/gameServer/tokenProving/").
-define(REQ_CORD,"D421D3CA1EEE46CA9001BE2D59CBBB24").
-define(AUC,"eyJhbGciOiJIUzUxMiIsInppcCI6IkRFRiJ9.eNosyjsOwyAQANG7bG0kFhYD7sznIIC3cKrI2FakKHcPkVJM8zRveJw7LFCbk7rwJqplK4hNEV5WJZyubaPmXJsRJuhXHXMihUnHFXPONMfVS4khq2R8DCEoGuPe-xivzocY3Xz8rJywoCGntLaIE_Dr-QeDAz5fAAAA__8.-YlD6A9gQqV_xc8jvQMvH9GPqXfpZ4-D5UbfVavCz5X1Mgt5YwXFv42EQG2CgDOd48eTS8zGfPtl-kr6USI2Pg").
-export([test/0,do_msg/1,check_acc/2,delet_info/0,tourist_login/1]).


-define(Headers,[
    {<<"content-length">>, integer_to_binary(length(Body))},
    {<<"content-type">>, "application/json"}
	]).

%% ====================================================================
%% Internal functions
%% ====================================================================

test()->
%% 	URL=?HTTP_URL++?REQ_CORD,
	DDD="2bc77263b7fd9bce8368a193cb3809890938d3af",
	{ok,Js}=tourist_login(DDD),
	jsx:decode(Js).


tourist_login(Device_io)->
	Path="/user/gameServer/visitorLogin/"++Device_io,
	try
		send_get(Path)
	catch 
		E:R ->?log("Eorror=~p~n,R=~p~n error=~p~n",[E,R,erlang:get_stacktrace()]),
			  no_data
	end.
check_acc(AppId,Token)->
	
	Path=?HTTP_URL,
	Body="{\"AppId\": \""++AppId++"\","++"\"Authorization\":\""++Token++"\"}",
	try
		send_post(Path, ?Headers, Body)
	catch 
		E:R ->?log("Eorror=~p~n,R=~p~n error=~p~n",[E,R,erlang:get_stacktrace()]),
			  no_data
	end.
game_record()->ok.

send_get(Path)->	
	  {ok, ConnPid} = gun:open(?HTTP_HOST, ?HTTP_PORT),
      {ok, _Protocol} = gun:await_up(ConnPid),
	     StreamRef = gun:get(ConnPid,Path),
	     case gun:await(ConnPid, StreamRef) of
	        {response, fin, _Status, _Headers} ->
	            no_data;
	        {response, nofin, _Status, _Headers} ->
	            {ok, Body} = gun:await_body(ConnPid, StreamRef),
	           	{ok, Body}
	    end.
send_post(Path, Headers, Body)->	
	  ?debug("Path=~p,Body=~p",[Path,Body]),
	  {ok, ConnPid} = gun:open(?HTTP_HOST, ?HTTP_PORT),
      {ok, _Protocol} = gun:await_up(ConnPid),
	     StreamRef = gun:post(ConnPid, Path, Headers, Body),
	     case gun:await(ConnPid, StreamRef) of
	        {response, fin, _Status, _Headers} ->
	            no_data;
	        {response, nofin, _Status, _Headers} ->
				
	            {ok, ResBody} = gun:await_body(ConnPid, StreamRef),
				{ok, ResBody} 
	    end.

do_msg({init})->
	{ok,Ip}=application:get_env(http_ip),
	{ok,Port}=application:get_env(game_port),
	Path="/user/gameServer/addGameServerAddr",
	Body="{\"ip\": \""++Ip++"\","++"port:"++util:to_list(Port)++"}",
	send_post(Path, ?Headers, Body);




do_msg({test})-> 
	test();



do_msg(Msg)->
	?debug("Msg=~p~n",[Msg]).


delet_info()->
	Ip="192.168.0.22",
	Port=8000,
	Path="/user/gameServer/delGameServerAddr",
	Body="{\"ip\": \""++Ip++"\","++"port:"++util:to_list(Port)++"}",
	send_post(Path, ?Headers, Body).