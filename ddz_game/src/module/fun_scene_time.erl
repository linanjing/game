%% @author Administrator
%% @doc @todo Add description to fun_scene_time.


-module(fun_scene_time).

-include("common.hrl").
%% ====================================================================
%% API functions
%% ====================================================================
-export([get_fapai_time/0,
		 get_two_game_interval_time/0,
		 get_kaishixiazhu_texiao_time/0,
		 get_xiazhu_time/0,
		 get_tingzhixiazhu_time/0,
		 get_all_open_card_time/0,
		 get_kaipaitexiao_time/0,
		 get_kapaijiange_time/0,
		 get_open_card_time/0]).



%% ====================================================================
%% Internal functions
%% ====================================================================
get_fapai_time()->
	Config=data_niuniu:get_data(2),
	Config#st_niuniu_config.fapai_time.


get_two_game_interval_time()->
	Config=data_niuniu:get_data(2),
	Config#st_niuniu_config.jiesuan_time.

get_kaishixiazhu_texiao_time()->
	Config=data_niuniu:get_data(2),
	Config#st_niuniu_config.kaishixiazhutexiao_time.

get_xiazhu_time()->
	Config=data_niuniu:get_data(2),
	Config#st_niuniu_config.xiazhu_time.

get_tingzhixiazhu_time()->
		Config=data_niuniu:get_data(2),
	Config#st_niuniu_config.tingzhixiazhutexiao_time.

get_all_open_card_time()->
		Config=data_niuniu:get_data(2),
	    T=Config#st_niuniu_config.all_time,
		T*4.
get_open_card_time()->
		Config=data_niuniu:get_data(2),
	    Config#st_niuniu_config.all_time.

get_kaipaitexiao_time()->
	Config=data_niuniu:get_data(2),
	 Config#st_niuniu_config.kaipaitexiao_time.

get_kapaijiange_time()->
	Config=data_niuniu:get_data(2),
	 Config#st_niuniu_config.kapaijiange_time.
