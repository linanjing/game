%% @author Administrator
%% @doc @todo Add description to fun_club_card.


-module(fun_club_card).

%% ====================================================================
%% API functions
%% ====================================================================
-include("common.hrl").
-export([get_card_config/0,init/0,test/0,oprate_card/1,get_club_usr_card_info/1,get_club_usr_card_benifit/1,
		 update_rebate_record/4,get_club_usr_card_rebate/1,update_benifit_record/4,bind_card/2]).

-define(REBATE,"../config_data/rebate.json").
-define(Club_card_config,club_card_config).
-record(club_card_config,{id=0,creater=0,platform=0,rebate=0}).
%% ====================================================================
%% Internal functions
%% ====================================================================
test()->
	{Date,_}=util:date(),
	?debug("Date=~p",[Date]).
%% 	util:timestamp_to_datetime(?NOW).
%% 	A=util:get_relative_day(24),
%% 	?debug("A=~p",[A]),
%% 	B=util:unixtime_to_relative_day(?NOW, 24),
%% 	?debug("B=~p",[B]).
	
init()->
	db:load_all(us_club_card),
	db:load_all(us_game_record_benifit),
	db:load_all(us_game_record_card),
	ConfigInfo=get_card_config(),
	erlang:put(?Club_card_config, ConfigInfo).

get_card_config()->
	[[{<<"data">>,ConfigList}]]=jsx:consult(?REBATE),
	F=fun(DataList)->
			 [Id,Creater,Platform,Rebate]=util:get_values([<<"id">>,<<"creater">>,
														   	<<"platform">>,<<"rebate">>],
														   DataList),
			 #club_card_config{id=Id,creater=Creater,platform=Platform,rebate=Rebate}
	  	end,
	lists:map(F, ConfigList).

get_club_usr_card_info({{ClubInfoId,Page},Mid,Sid})->
	?debug("Mid=~p",[Mid]),
	case db:dirty_get(us_club, ClubInfoId, #us_club.club_info_id) of
		[#us_club{flow_card=CardId,game_uid=Mid}]->					 
			case db:dirty_get(us_club_card, CardId, #us_club_card.club_card_id) of
				[#us_club_card{benefits=Benifit,rebate=Rebate,club_user_bear=Club_user_bear}]->
					MemberInfo=db:dirty_match(us_club_member, #us_club_member{_='_',
															  club_info_id=ClubInfoId	}),
					?debug("MemberInfo=~p",[MemberInfo]),
					SendMbember=util:get_list_page(MemberInfo, Page, 40),
					F=fun(#us_club_member{game_uid=Uid,interger=Interger})->
							  Name=fun_login:get_usr_name(Uid),
							  Online=case db:dirty_get(ply, Uid) of
										 [#ply{}]->1;
										 _->0
									 end,
							  #pt_member_card_info{uid=Uid,name=Name,
												   headId=10,
												   is_online=Online,
												   interger=Interger,
												   proportion=Club_user_bear,
												   benifit=Benifit,
												   rebate=util:ceil(Rebate)} end,
					Pt=lists:map(F, SendMbember),
					pt:send(Sid, ?PT_RES_CLUB_USER_CARD_INFO, {Pt});
				_->?debug("1111111111"),skip
			end;
		_->?debug("1111111111"),skip
	end.
bind_card(Uid,CardId)->
	db:insert(#us_club_card{uid=Uid,club_card_id=CardId,club_user_bear=0,platform_bear=0,rebate=20}).
					
oprate_card({{Club_Info_Id,Uid,ClubCardConfigId},MyId,Sid})->
	?debug("11111111111111"),
	ConfigInfo=erlang:get(?Club_card_config),
	case lists:keyfind(ClubCardConfigId, #club_card_config.id, ConfigInfo) of
		#club_card_config{id=ClubCardConfigId,
						  creater=Creater,
						  platform=Platform,
						  rebate=Rebate}->
			case db:dirty_get(us_club, Club_Info_Id, #us_club.club_info_id) of
				[#us_club{flow_card=CardId,game_uid=MyId}]->
					case fun_club:check_member(Club_Info_Id, Uid) of
						true->
							?debug("Uid=~p,CardId=~p",[Uid,CardId]),
							case db:dirty_match(us_club_card, #us_club_card{_='_',uid=Uid}) of
								CardInfoList->
									?debug("CardInfoList=~p",[CardInfoList]),
									F=fun(#us_club_card{club_card_id=McardId})->
											  ?debug("McardId=~p",[McardId]),
											  McardId==CardId end,
									case lists:filter(F, CardInfoList) of
										[CardInfo]->
									?debug("CardInfo=~p",[CardInfo]),
									db:dirty_put(CardInfo#us_club_card{club_user_bear=Creater,platform_bear=Platform,
																	   rebate=Rebate}),
									?debug("11111111111111"),
									pt:send(Sid, ?PT_RES_CLUB_CHANGE_PER, {Club_Info_Id,Uid,ClubCardConfigId});		
										_->	?debug("11111111111111"),skip
									end;
							_R->?debug("_R=~p",[_R]),skip
							
							end;
						_->?debug("11111111111111"),skip
					end;
				_->?debug("11111111111111"),skip
			end;
		_->?debug("11111111111111"),skip
	end.
get_club_usr_card_rebate({{ClubInfoId,Page},_Mid,Sid})->
	case db:dirty_match(us_game_record_card, #us_game_record_card{_='_',club_info_id=ClubInfoId}) of
		List when length(List)>0->
		NeedList=util:get_list_page(List, Page, 10),
		pt:send(Sid, ?PT_RES_BET_CARD_INFO, {NeedList});
		[]->pt:send(Sid, ?PT_RES_BET_CARD_INFO, {[]})
	end.
get_club_usr_card_benifit({{ClubInfoId,Page},_Mid,Sid})->
	case db:dirty_match(us_game_record_card, #us_game_record_benifit{_='_',club_info_id=ClubInfoId}) of
		List when length(List)>0->
		NeedList=util:get_list_page(List, Page, 10),
		pt:send(Sid, ?PT_RES_BET_BENIFIT_INFO, {NeedList});
		[]->pt:send(Sid, ?PT_RES_BET_BENIFIT_INFO, {[]})
	end.
update_rebate_record(Uid,Bet,ClubInfoId,BetRebate)-> 
	?debug("update_rebate_record==============="),
	{{Y,M,D},_}=util:date(), 
	Date=util:to_binary((integer_to_list(Y)++"-"++integer_to_list(M)++"-"++integer_to_list(D))),
	[#ply{name=Name}]=db:dirty_get(ply, Uid),
	case db:dirty_match(us_game_record_card, #us_game_record_card{_='_',id=Uid,date=Date,club_info_id=ClubInfoId}) of
		[Recored=#us_game_record_card{bet=OldBet,rebate=OldBetRebate}]->
		db:dirty_put(Recored#us_game_record_card{bet=OldBet+Bet,rebate=OldBetRebate+BetRebate,time=?NOW});
		[]->db:insert_with_id(#us_game_record_card{id=Uid,name=util:to_binary(Name),
												   time=?NOW,bet=Bet,rebate=BetRebate,
												   date=Date,club_info_id=ClubInfoId})
	end.
		
update_benifit_record(Uid, CLubInfoId,UpdateCoin, ClubUpdate)->
	{{Y,M,D},_}=util:date(),
	Date=util:to_binary((integer_to_list(Y)++"-"++integer_to_list(M)++"-"++integer_to_list(D))),
	[#ply{name=Name}]=db:dirty_get(ply, Uid),
	case db:dirty_match(us_game_record_benifit, #us_game_record_benifit{_='_',id=Uid,
																		date=Date,
																		club_info_id=CLubInfoId}) of
		[Recored=#us_game_record_benifit{proportion=OldUpdateCoin,rebate=OldClubUpdate}]->
			db:dirty_put(Recored#us_game_record_benifit{proportion=OldUpdateCoin+UpdateCoin,
														rebate=OldClubUpdate+ClubUpdate,time=?NOW});
		[]->
			db:insert_with_id(#us_game_record_benifit{id=Uid,name=util:to_binary(Name),time=?NOW,proportion=UpdateCoin,
												 rebate=ClubUpdate,date=Date,club_info_id=CLubInfoId})
	end.

