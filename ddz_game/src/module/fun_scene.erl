%% @author Administrator
%% @doc @todo Add description to fun_scene.


-module(fun_scene).
-include("common.hrl").
%% ====================================================================
%% API functions
%% ====================================================================
-export([do_msg/1]).



%% ====================================================================
%% Internal functions
%% ====================================================================
do_msg({enter_scene,Uid,Sid,Name,HeadId,CardInfo,SceneCoinInfo})->
%% 	{enter_scene,Uid,Sid,"玩家21",11,1000000}
	Module=erlang:get(module),
	Module:join_game({Uid,Sid,Name,HeadId,CardInfo,SceneCoinInfo}); 
do_msg({pt,Func,{Data,Uid,Sid}})->
	Module=erlang:get(module),
	?debug("Module=~p,Func=~p,Data=~p,Uid=~p,Sid=~p",[Module,Func,Data,Uid,Sid]),
	Module:Func({Data,Uid,Sid});
do_msg({process_pt,Module,Func,Args})->
	?debug("Module=~p,Func=~p,Data=~p",[Module,Func,Args]),
	Module:Func(Args);
do_msg({process_pt,Module,Func})->
	Module:Func();
do_msg({game_over})->
	fun_scene_lh:game_over();
do_msg({bet_controller,Num})->
	fun_scene_lh:bet_controller(Num);
do_msg({game_process})->
	fun_scene_lh:game_process();
do_msg({movie})->
	fun_scene_lh:movie();
do_msg({broad_status_stop})->
	fun_scene_lh:broad_status_stop();
do_msg({tcp_closed,Uid})->
	fun_scene_lh:leave_lh(Uid),
	ok;

do_msg({broad_status})->
	fun_scene_lh:broad_status(),
	ok;	
do_msg(Msg)->
	?debug("Msg=~p~n",[Msg]).

