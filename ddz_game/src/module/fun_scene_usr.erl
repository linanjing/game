%% @author Administrator
%% @doc @todo Add description to fun_scene_usr.


-module(fun_scene_usr).
-include("common.hrl").
%% ====================================================================
%% API functions
%% ====================================================================
-export([get_player_data/0,get_status/0]).



%% ====================================================================
%% Internal functions
%% ====================================================================


get_player_data()->
	Robots=erlang:get(robots),
	UsrFinalInfo=
		case erlang:get(usr) of
			UsrList when length(UsrList)>0->
				F=fun(Uid,L)->
						  #player{coin=Coin,head_id=Head,name=Name}=
							  erlang:get(Uid),	
						  L++[{Uid,Coin,Head,Name}] end,
				lists:foldl(F, [], UsrList);
			_->[]
		end,
	List=get_robot_start_info(Robots,6), 
	?debug("List=~p",[List]),
	List++UsrFinalInfo.


get_robot_start_info(Robots,Num)->
	F=fun(_KEY,#player{id=Uid,coin=Coin,head_id=Head,name=Name},R)-> 
			  R++[{Uid,Coin,Head,Name}] end,
	L=dict:fold(F, [], Robots),
	lists:sublist(L, Num).


get_status()->
	case erlang:get(?GAME_STATUS) of
		stop->
			0;
		start->
			1
	end.
