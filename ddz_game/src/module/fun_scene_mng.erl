%% @author Administrator
%% @doc @todo Add description to fun_scene_mng.


-module(fun_scene_mng).
-include("common.hrl").
%% ====================================================================
%% API functions
%% ====================================================================
-export([scene_add/3,scene_add/5]).






scene_add(MasterName,ClubId,Uid,Sid,SceneType)->
	SceneId=fun_scene_select:get_scene_id(),
	?debug("club creater init scene SceneId=~p,Type=~p",[SceneId,SceneType]),
	Pt=[#pt_table_info{club_id=ClubId,game_type=SceneType,table_id=SceneId}], 
	Admin=#admin_info{adminUid=Uid,current_admin_currency=0,adminName=MasterName,adminhead=10},
	Data={[{sid,Sid},{club_info,Pt},{?SceneCurrency,?Interger},{?CurrencyType,3},{ptadmin,Admin}],SceneId,SceneType},
	gen_server:cast(self(), {create_scene,Data}).	

scene_add(SceneData,CurrencyType,SceneType)->
	case fun_platform_admin:get_table_admin_info(CurrencyType) of
		Admin=#admin_info{}->
			SceneId=fun_scene_select:get_scene_id(),
			?debug("flatform init scene SceneId=~p,Type=~p,CurrencyType=~p",[SceneId,SceneType,CurrencyType]),
			Data={SceneData++[{ptadmin,Admin}],SceneId,SceneType},
			gen_server:cast(self(), {create_scene,Data});
		no->skip
%% 			SceneId=fun_scene_select:get_scene_id(),
%% 			?debug("flatform init scene SceneId=~p,Type=~p,CurrencyType=~p",[SceneId,SceneType,CurrencyType]),
%% 			Data={[{?SceneCurrency,?Coin},{?CurrencyType,1},{ptadmin,no}],SceneId,1},
%% 			gen_server:cast(self(), {create_scene,Data})
	end.