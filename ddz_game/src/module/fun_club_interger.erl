%% @author Administrator
%% @doc @todo Add description to fun_club_interger.


-module(fun_club_interger).
-include("common.hrl").
-define(ADD_IN,1).
-define(REDUCE_IN,2).

%% ====================================================================
%% API functions
%% ====================================================================
-export([add_integer/1,update_integer/6,update_integer_creater/4,get_interger_info/1,get_interger_update/1,test/0]).



%% ====================================================================
%% Internal functions
%% ====================================================================
test()->
	Date=get_date(),
	db:dirty_match(us_game_record_amount, #us_game_record_amount{_='_',uid=100021,
																		time_str=Date,
																		scene_id=200001,
																		scene_type=2,
																		club_info_id=100005}).

get_interger_info({{ClubInfoId,Page},Uid,Sid})->
	case fun_club:check_leader(ClubInfoId, Uid) of
		true->
			List=db:dirty_match(us_game_record_amount_creater, #us_game_record_amount_creater{_='_',club_info_id=ClubInfoId}),
			Need=util:get_list_page(List, Page, 10),
			F=fun(#us_game_record_amount_creater{time=Time,scene_id=SceneId,scene_type=SceneType,end_amount=End_interger})->
					  #pt_club_game_info{time=Time,scene_type=SceneType,scene_id=SceneId,update=End_interger} end,
			PtList=lists:map(F, Need),
			pt:send(Sid, ?PT_RES_LEADER_INTERGER_INFO, {PtList});
		_->
			case fun_club:check_member(ClubInfoId, Uid) of
				true->
					List=db:dirty_match(us_game_record_amount, #us_game_record_amount{_='_',uid=Uid,club_info_id=ClubInfoId}),
					Need=util:get_list_page(List, Page, 10),
					F=fun(#us_game_record_amount{time=Time,scene_type=SceneType,scene_id=SceneId,amount=End_interger})->
							   #pt_club_game_info{time=Time,scene_type=SceneType,scene_id=SceneId,update=End_interger} end,
					PtList=lists:map(F, Need),
					pt:send(Sid, ?PT_RES_LEADER_INTERGER_INFO, {PtList});
				_->skip
			end
	end.

add_integer({{Type,Uid,Interger,ClubInfoId},LeaderId,Sid})->
	?debug("Type,Uid,Interger,ClubInfoId =~p",[{Type,Uid,Interger,ClubInfoId}]),
	case fun_club:check_leader(ClubInfoId, LeaderId) of
		true->
			if
				Interger=/=Uid->
%% 			R=db:dirty_match(us_club_member, #us_club_member{_='_',game_uid=Uid,club_info_id=ClubInfoId}),
%% 			?debug("R=~p",[R]),
			case db:dirty_match(us_club_member, #us_club_member{_='_',game_uid=Uid,club_info_id=ClubInfoId}) of
				[]->skip;
				[Member=#us_club_member{interger=Old}]->
					case Type of
						?ADD_IN ->
							db:dirty_put(Member#us_club_member{interger=Old+Interger}),
							pt:send(Sid, ?PT_LEADER_RES_UPDATE_INTERGER, {ClubInfoId,Uid,Interger,Old+Interger}),
							case db:dirty_get(ply, Uid) of
								[#ply{sid=MSid}]->
									pt:send(MSid, ?PT_RES_UPDATE_INTER, {ClubInfoId,Interger,Old+Interger});
								_->skip
							end;
						?REDUCE_IN->
							End=if
									Old-Interger>=0->Old-Interger;
									true->0
								end,					
							db:dirty_put(Member#us_club_member{interger=End}),
							pt:send(Sid, ?PT_LEADER_RES_UPDATE_INTERGER, {ClubInfoId,Uid,Interger,End}),
								case db:dirty_get(ply, Uid) of
								[#ply{sid=MSid}]->
									pt:send(MSid, ?PT_RES_UPDATE_INTER, {ClubInfoId,Interger,End});
								_->skip
							end,
						db:insert(#us_game_interger_update{time=?NOW,club_info_id=ClubInfoId,uid=Uid,interger=Interger});
						_R->?debug("_R=~p",[_R]),skip
					end;
				_R->?debug("_R=~p",[_R])
			
			end;
				true->skip
			end;
		_->skip
	end.
get_interger_update({{ClubInfoId,Uid},Leader,Sid})->
	case fun_club:check_leader(ClubInfoId, Leader) of
		true->
			List=db:dirty_match(us_game_interger_update, #us_game_interger_update{_='_',club_info_id=ClubInfoId,uid=Uid}),
			pt:send(Sid, ?PT_RES_INTERGER_RECORD, {List});
		_->skip
	end.
update_integer_creater(SceneId,SceneType,_CLubInfoId,End)->
	case db:dirty_get(scene, SceneId) of
		[#scene{club_id=CLubInfoId}]->
	?debug("update_integer_creater=========="),
			Date=get_date(),
			case db:dirty_match(us_game_record_amount_creater, #us_game_record_amount_creater{_='_',
																								time_str=Date, 
																								scene_id=SceneId,
																								scene_type=SceneType,
																								club_info_id=CLubInfoId}) of
				[]->
					?debug("======================"),
					db:insert(#us_game_record_amount_creater{scene_id=SceneId,time_str=Date,end_amount=End,time=?NOW,
															  scene_type=SceneType,club_info_id=CLubInfoId});
				[Record=#us_game_record_amount_creater{}]->
					?debug("======================"),
					db:dirty_put(Record#us_game_record_amount_creater{end_amount=End})
			end;
		_->skip
	end.
			
update_integer(Uid,Name,SceneId,SceneType,Update,CLubInfoId)->
		?debug("update_integer_creater=========="),
	Date=get_date(),
	?debug("Name=~p",[Name]),
	case db:dirty_match(us_game_record_amount, #us_game_record_amount{_='_',uid=Uid,
																		time_str=Date,
																		scene_id=SceneId,
																		scene_type=SceneType,
																		club_info_id=CLubInfoId}) of
		[]->
			?debug("insert_integer=========="),
			db:insert(#us_game_record_amount{uid=Uid,time=?NOW,name=unicode:characters_to_binary(Name),
													  scene_id=SceneId,scene_type=SceneType,
		 											  amount=Update,time_str=Date,
													  club_info_id=CLubInfoId});
		[Record=#us_game_record_amount{amount=Old}]->
			?debug("update_integer_creater=========="),
			db:dirty_put(Record#us_game_record_amount{time=?NOW,amount=Update+Old})
	end.

get_date()->
	{{Y,M,D},_}=util:date(),
	Date=unicode:characters_to_binary((integer_to_list(Y)++"-"++integer_to_list(M)++"-"++integer_to_list(D))),
	?debug("Date=~p",[Date]),
	Date.
