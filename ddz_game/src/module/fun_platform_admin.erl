%% @author Administrator
%% @doc @todo Add description to fun_platform_admin.


-module(fun_platform_admin).
-include("common.hrl").
%% ====================================================================
%% API functions
%% ====================================================================
-export([init/0,get_table_admin_info/1,flowcard_rebate/2]).

%% -define(AdminType,<<"4">>).
%% -define(AdminUid,adminUid).
%% -define(AdminCurrency,adminCurrency).
%% -define(AdminName,adminName).
%% -define(AdminHeadId,adminhead).


%% ====================================================================
%% Internal functions
%% ====================================================================
-define(TableCurrency,5000000).


init()->
	db:load_all(us_account),
	db:load_all(us_user_info),
	[#us_user_info{user_game_id=Uid,nick_name=Name,user_id=User_id}]=db:dirty_match(us_user_info, #us_user_info{_='_',user_type=?AdminType}),
	[#us_account{amount=Currency}]=db:dirty_match(us_account, #us_account{_='_',user_id=User_id}),
	?debug("Currency=~p,Name=~p,Uid=~p",[Currency,Name,Uid]),
	erlang:put(admin_info, #admin_info{adminUid=Uid,all_adminCurrency=Currency,adminName=Name,adminhead=10}),
%% 	erlang:put(?AdminUid, Uid),
%% 	erlang:put(?AdminName, Name),
%% 	erlang:put(?AdminCurrency, Currency),
%% 	erlang:put(?AdminHeadId, 10),
	ok.


get_table_admin_info(2)->
	AdminInfo=#admin_info{adminUid=Uid,all_adminCurrency=Currency,adminName=Name,adminhead=10}=erlang:get(admin_info),
	if
		Currency>?TableCurrency->
			All=((Currency*100)-(?TableCurrency*100))/100,
			?debug("All=~p",[All]),
			erlang:put(admin_info, AdminInfo#admin_info{all_adminCurrency=All}),
 		#admin_info{adminUid=Uid,current_admin_currency=?TableCurrency,adminName=Name,adminhead=10};
		true->
			no
	end;
get_table_admin_info(_)->no.


flowcard_rebate(LeaderId,Num)->
		AdminInfo=#admin_info{all_adminCurrency=Currency}=erlang:get(admin_info),
		All=((Currency*100)-(Num*100))/100,
	   	erlang:put(admin_info, AdminInfo#admin_info{all_adminCurrency=All}),
		fun_ply:update_currency(LeaderId, Num).
		

