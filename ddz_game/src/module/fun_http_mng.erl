%%%-------------------------------------------------------------------
%%% @author suyang
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 28. 十一月 2018 17:48
%%%-------------------------------------------------------------------
-module(fun_http_mng).
-author("suyang").

-include("common.hrl").

%% API
-export([do_init/0,do_close/0,do_call/1,do_info/1,do_time/1,do_msg/1]).

do_init() -> ok.

do_close() -> ok.

do_msg({wechat_login_req, Sid, Code})->
    ?debug("fun_http_mng"),
    login:check_player_info(Code, Sid);
do_msg(Msg)->
    ?debug("Msg=~p~n",[Msg]).

do_call(Msg)->
    ?debug("Msg=~p~n",[Msg]).

do_info(Msg)->
    ?debug("Msg=~p~n",[Msg]).

do_time(Msg)->
    ?debug("Msg=~p~n",[Msg]).