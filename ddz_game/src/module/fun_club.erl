%% @author Administrator
%% @doc @todo Add description to fun_club.


-module(fun_club).

%% ====================================================================
%% API functions
%% ====================================================================

-include("common.hrl").
-export([create_club/1,search_club/1,test/0,req_enter_club/1,req_ask_join_list/1,add_club_member_info/4,leader_req_ask_join_list/1,
		 req_search_club_info/1,verify_usr/1,leader_oprate_usr/1,init/0,req_create_table/1,req_table_list/1,get_club_info_id/1]).
-export([check_leader/2,get_card_info/1,get_club_card/1,choose_card/1,check_member/2]).

init()->
	db:load_all(us_club),	
	db:load_all(us_club_member),
	db:load_all(us_game_record_amount),
	db:load_all(us_game_record_amount_creater),
	ok.
test()->
		ok.
choose_card({{CardId},Uid,Sid})->
	case db:dirty_get(us_club_card, CardId, #us_club_card.club_card_id) of
		[#us_club_card{}]->
			case db:dirty_get(us_club, CardId, #us_club.flow_card) of
				[#us_club{club_info_id=InfoId}]->
					case db:dirty_match(us_club_member, #us_club_member{_='_',club_info_id=InfoId,game_uid=Uid}) of
						[Member=#us_club_member{}]->
							db:dirty_put(Member#us_club_member{card=CardId}),						
							case db:dirty_get(ply, Uid) of
								[Ply=#ply{}]->
									db:dirty_put(Ply#ply{club_card_id=CardId}),				
									pt:send(Sid, ?PT_RES_CHOOSE_CLUB_CARD_INFO, {1});
								_->pt:send(Sid, ?PT_RES_CHOOSE_CLUB_CARD_INFO, {0})
							end;
						_->skip
					end;
				_->skip
			end;
		_->skip
	end.
get_card_use(Uid,CardId)->
	case db:dirty_get(ply, Uid) of
		[#ply{club_card_id=MyId}]->if
									   MyId==CardId->1;
									   true->0
								   end;
		_->0
	end.
	
get_club_card({{},Uid,Sid})->
	case db:dirty_get(us_club_member, Uid, #us_club_member.game_uid) of
		[]->
			pt:send(Sid, ?PT_RES_CLUB_CARD_INFO, {[]});
		List ->
			F=fun(#us_club_member{club_info_id=InfoId},L)->
					  case db:dirty_get(us_club, InfoId, #us_club.club_info_id) of
						  [#us_club{flow_card=CardInfoId,club_name=Club_name,game_uid=LeaderId}]->
							  Use=get_card_use(Uid,CardInfoId),
							  [ #us_club_member{name=LeaderName}]=
								  db:dirty_match(us_club_member, #us_club_member{_='_',game_uid=LeaderId}),
							  if
								 CardInfoId=/=0-> 
							  	L++[#pt_club_card_info{id=CardInfoId,club_name=Club_name,
													 create_name=LeaderName,use=Use}];
								true->L
							end;
						  _->L
						end end,
			PtList=lists:foldl(F, [], List),
			pt:send(Sid, ?PT_RES_CLUB_CARD_INFO, {PtList})
	end.
						  
get_card_info(CardInfoId)->
	?debug("CardInfoId=~p",[CardInfoId]),
%% 	case db:dirty_get(us_club, ClubInfoId, #us_club.club_info_id) of
%% 		[#us_club{flow_card=CardInfoId}]->
			case db:dirty_get(us_club_card, CardInfoId, #us_club_card.club_card_id) of
				[#us_club_card{rebate=Rebate,
							   club_user_bear=Club_user_bear,
							   platform_bear=Platform_bear,
							   benefits=Benefits}]->
					%%Benefits 保险金  
				#club_card_info{club_card_id=CardInfoId,
								rebate=Rebate,
								club_user_bear=Club_user_bear,
								platform_bear=Platform_bear,
								benefits=Benefits};
				_->#club_card_info{club_card_id=CardInfoId}
			end.
%% 		_->#club_card_info{club_info_id=ClubInfoId}
%% 	end.
				
check_member(ClubId,Uid)->
	case db:dirty_match(us_club_member, #us_club_member{_='_',club_info_id=ClubId,game_uid=Uid,player_state=1}) of
		[]->false;
		_->true
	end.
get_club_info_id(Uid)->
	case db:dirty_get(us_club_member, Uid,#us_club_member.game_uid) of
		[]->0;
		List when erlang:length(List)>0-> 
			[#us_club_member{club_info_id=ClubInfoId}|_]=List,
			ClubInfoId
	end.

req_create_table({{ClubInfoId,GameType},Uid,Sid})->
	case db:dirty_get(us_club, ClubInfoId, #us_club.club_info_id) of
		[#us_club{game_uid=Uid}]->
				Name=fun_login:get_usr_name(Uid),	
				fun_scene_mng:scene_add(Name,ClubInfoId,Uid, Sid,GameType);
		_->?debug("111111111"),skip
	end.

req_table_list({{ClubInfoId},_Uid,Sid})->
	SceneList= db:dirty_match(scene, #scene{_='_',club_id=ClubInfoId}),
	?debug("table=~p",[SceneList]),
	F=fun(#scene{id=SceneId,type=Type,club_id=Club_id},L)->
					  if
						  ClubInfoId==Club_id ->
							  L++[#pt_table_info{club_id=ClubInfoId,game_type=Type,table_id=SceneId}];
						  true->L
					  end end,
	Pt=lists:foldl(F, [], SceneList),
		?debug("Pt=~p",[Pt]),
	pt:send(Sid, ?PT_RES_TABLE_LIST, {Pt}).
	
			
create_club({{Name},Uid,Sid})->
	case db:dirty_get(ply, Uid) of
		[#ply{name=UName}]->
		case db:dirty_get(us_club, list_to_binary(Name), #us_club.club_name) of
				[]->
					Now=util:unixtime(),
					[Club=#us_club{id=Club_Id}]=db:insert(#us_club{club_name=list_to_binary(Name),
														game_uid=Uid,create_time=Now,player_num=1}),
					db:dirty_put(Club#us_club{club_info_id=100000+Club_Id,flow_card=100000+Club_Id}), %%临时需求,直接给绿卡
					Club_info_id=100000+Club_Id,
					add_club_member_info(Uid,Club_info_id,1,UName),
%%  					create_club_card(100000+Club_Id),
%% 					fun_club_card:bind_card(Uid, 100000+Club_Id),
					Info=#pt_guild_info{id=Club_info_id,name=list_to_binary(Name),
										 my_open_game=[],
										 online_player_num=1,
										 all_player_num=1,
										 creater_id=Uid,
										 myreq_state=1,
										 guild_head=10},
					    pt:send(Sid, ?PT_RES_CREATE_GUILD, {1,"",[Info]});
				_->	pt:send(Sid, ?PT_RES_CREATE_GUILD, {0,"创建失败",[]})
		end;
		_->skikp
	end.

verify_usr({{ClubId,Uid,State},_LeaderId,Sid})->
			ReqState=
				case State of
					1->1;
					_->0
				end,
			case db:dirty_get(us_club, ClubId, #us_club.club_info_id) of
				[#us_club{flow_card=CardId}]->
					case db:dirty_match(us_club_member, #us_club_member{_='_',club_info_id=ClubId,game_uid=Uid}) of
				[]->skip;
				[Member=#us_club_member{player_state=Old}] when Old=/=1->
					Now=util:unixtime(),
					db:dirty_put(Member#us_club_member{player_state=State,enter_time=Now}),
					case State of
						1->
							fun_club_card:bind_card(Uid, CardId);
						0->skip
					end,
					?debug("State=~p",[State]),					
					Data={ClubId,Uid,ReqState},
					pt:send(Sid, ?PT_LEADER_RES_VERIFY_USER, Data);	
				_->?debug("1111111"),skip
					end;
				_->skip
			end,
			case db:dirty_get(ply, Uid) of
						[#ply{sid=MSid}]->							
							Info=get_club_info(ClubId,Uid),
							MData={ClubId,ReqState,Info},
							pt:send(MSid, ?PT_LEADER_RES_VERIFY_TO_USER, MData);
						_->?debug("1111111"),skip
					end.

get_club_info(ClubId,MyId)->
	case db:dirty_get(us_club, ClubId, #us_club.club_info_id) of
		[#us_club{club_name=Name,game_uid=Uid}]->
			AllMember=db:dirty_match(us_club_member, #us_club_member{_='_',club_info_id=ClubId,player_state=1}),
			F=fun(#us_club_member{game_uid=Uid})->
					case db:dirty_get(ply,Uid) of
						[#ply{online=1}]->true;
						_->false
					end end,
			Need=lists:filter(F,AllMember),
			All_num=erlang:length(AllMember),
			Online=erlang:length(Need),
			{In,Cardex}=
			case db:dirty_match(us_club_member, #us_club_member{_='_',club_info_id=ClubId,game_uid=MyId,player_state=1})of
				[#us_club_member{interger=Inc,card=Card}]->{Inc,Card};
				_->0
			end,
			[#pt_guild_info{id=ClubId,name=util:to_binary(Name),
										 my_open_game=[],
										 online_player_num=Online,
										 all_player_num=All_num,
										 creater_id=Uid,
										 myreq_state=1,
										 guild_head=0,
										 integer=In,
										card=Cardex}];
		_->[]
	end.
get_club_by_state(State,Uid)->
	db:dirty_match(us_club_member, #us_club_member{_='_',game_uid=Uid,player_state=State}).

	

search_club({{},Uid,Sid})->
		List1=get_club_by_state(1,Uid),
		List2=get_club_by_state(2,Uid),
		?debug("List1=~p,List2=~p",[List1,List2]),
		case List1++List2  of
			[]->	pt:send(Sid, ?PT_RES_GUILD_LIST, {[]});
			ClubList->
				F=fun(#us_club_member{club_info_id=Id,interger=In,player_state=State})->
						  [#us_club{club_info_id=Club_Id,club_name=Name,player_num=Player_Num,flow_card=Card,
								 game_uid=Creater_id}]=db:dirty_get(us_club, Id,#us_club.club_info_id),  
						        #pt_guild_info{id=Club_Id,name=Name,
										 my_open_game=[],
										 online_player_num=1,
										 all_player_num=Player_Num,
										 creater_id=Creater_id,
										 myreq_state=State,
										 guild_head=0,
										 card=Card,
										 integer=In	   
										 } end,
				GuildInfoList=lists:map(F, ClubList),
				?debug("ClubList=~p,GuildInfoList=~p",[ClubList,GuildInfoList]),
				pt:send(Sid, ?PT_RES_GUILD_LIST, {GuildInfoList})
		end.
get_req_state(Uid,Id)->
	case db:dirty_match(us_club_member, #us_club_member{_='_',club_info_id=Id,game_uid=Uid}) of
		[]->0;
		[#us_club_member{player_state=State}]->State
	end.
req_search_club_info({{Id},Uid,Sid})->
		case db:dirty_get(us_club, Id,#us_club.club_info_id) of
			[#us_club{club_name=Name,player_num=Player_Num,club_info_id=Id,
								 game_uid=Creater_id}]->
						State=get_req_state(Uid,Id),
						GuildInfoList=[  #pt_guild_info{id=Id,name=Name,
										 my_open_game=[],
										 online_player_num=1,
										 all_player_num=Player_Num,
										 creater_id=Creater_id,
										 myreq_state=State
										 } ],
						pt:send(Sid, ?PT_RES_SEARCH_GUILD, {1,GuildInfoList});
			_->pt:send(Sid, ?PT_RES_SEARCH_GUILD, {0,[]})
		end.

req_ask_join_list({{},Uid,Sid})->
	List=get_req_enter_info(Uid), 
	pt:send(Sid, ?PT_RES_ASK_ClUB_INFO, {List}).

req_enter_club({{ClubId},Uid,_Sid})->
	?debug("req_enter_club======"),
	case db:dirty_get(ply, Uid) of
		[#ply{name=Name}]->
			case db:dirty_match(us_club_member, #us_club_member{_='_',club_info_id=ClubId,game_uid=Uid}) of
				[]->
					?debug("req_enter_club======"),
					db:insert(#us_club_member{club_info_id=ClubId,game_uid=Uid,player_state=2,name=unicode:characters_to_binary(Name)});
				[Member=#us_club_member{player_state=State}] when State==5 orelse State==0 ->
					db:dirty_put(Member#us_club_member{player_state=2});
				_R->?debug("req_enter_club fail,_R=~p",[_R]),skip
			end;
		_->skip
	end.

add_club_member_info(Uid,Id,_State,Name)->
		
		case db:dirty_match(us_club_member, #us_club_member{_='_',club_info_id=Id,game_uid=Uid}) of
				[ClubRecord=#us_club_member{}]->
					db:dirty_put(ClubRecord#us_club_member{player_state=1,enter_time=?NOW});
				_->db:insert(#us_club_member{game_uid=Uid,club_info_id=Id,player_state=1,enter_time=?NOW,name=unicode:characters_to_binary(Name)})
			end.
get_club_name(Id)->
	case db:dirty_get(us_club, Id,#us_club.club_info_id) of
		[#us_club{club_name=Name}]->
			Name;
		_->""
	end.
get_req_enter_info(Uid)->
	case db:dirty_get(us_club_member, Uid,#us_club_member.game_uid) of
		List when erlang:is_list(List)->
			F=fun(#us_club_member{club_info_id=Id,player_state=State})->
					  Name=get_club_name(Id),
					  #pt_enter_club_info{id=Id,name=Name,head_id=0,state=State}
			  end,
			lists:map(F, List);
		_->[]
	end.

get_member_info(ClubId,Type,Page)->
	?debug("get_member_info=~p",[Type]),
	case db:dirty_match(us_club_member, #us_club_member{_='_',club_info_id=ClubId,player_state=Type}) of
		List when length(List)>0->
			?debug("List=~p",[List]),
			util:get_list_page(List, Page, 10);
		_->[]
	end.
 get_club_name_by_info(ClubId)->
	  case db:dirty_get(us_club,ClubId,#us_club.club_info_id) of
		[#us_club{club_name=Name}]->Name;
		  _->"工会"
	  end.
check_leader(ClubId,Uid)->
	?debug("check_leader Uid=~p,ClubId=~p,Info=~p",[Uid,ClubId,db:dirty_get(us_club,ClubId, #us_club.club_info_id) ]),
	  case db:dirty_get(us_club,ClubId, #us_club.club_info_id) of
		[#us_club{game_uid=Uid}]->true;
		  _R->?debug("_R=~p",[_R]),false
	  end.
leader_oprate_usr({{ClubId,Uid,State},LeaderId,Sid})->
	?debug("1111111111111111"),
	case check_leader(ClubId, LeaderId) of
		true->
			?debug("1111111111111111"),
		case db:dirty_match(us_club_member, #us_club_member{_='_',club_info_id=ClubId,game_uid=Uid}) of
			    [Member=#us_club_member{}]->
					?debug("State=~p",[State]),
					
					db:dirty_put(Member#us_club_member{player_state=State}),
					case db:dirty_get(ply, Uid) of
						[#ply{sid=USid}] when erlang:is_pid(USid)->
%% 							Info=get_club_info(ClubId),
							Name=get_club_name(ClubId),
							pt:send(USid, ?PT_LEADER_RES_FREEZE_TO_USER, {State,LeaderId,Name,ClubId});
						_->skip
					end,
					?debug("1111111111111111"),
					pt:send(Sid, ?PT_LEADER_RES_FREEZE_USER, {ClubId,Uid,State});
			_->?debug("1111111111111111"),skip
		end;
		_->?error_report(Sid,2),skip
	end.
leader_req_ask_join_list({{ClubId,Page,Type},ThisUid,Sid})->
	case check_member(ClubId, ThisUid) of
		true->
			RetList=get_member_info(ClubId,Type,Page),
			All=get_member_info(ClubId,Type,1),
			?debug("RetList=~p",[RetList]),
			F=fun(#us_club_member{game_uid=Uid,enter_time=Enter_time,interger=In,name=Name},L)->
		 					  ?debug("interger=~p",[In]),
							  Online=case db:dirty_get(ply, Uid) of
										 [#ply{}]->1;
										 _->0
									 end,
							  LastLoginTime=fun_login:get_last_login_time(Uid),
							  L++[#pt_club_member_info{id=Uid,name=Name,
													   head=10,online=Online,integral=In,
													   enter_time=Enter_time,game_num=0,
													   last_login_time=LastLoginTime}]
						 end,
			Pt=lists:foldl(F, [], RetList),
			All_Num=erlang:length(All),
			Online=erlang:length(Pt),
			?debug("Pt=~p",[Pt]),
			pt:send(Sid, ?PT_LEADER_RES_ASKJOIN_LIST, {ClubId,All_Num,Online,Pt,Type});
		_->?error_report(Sid,3)
	end.	   


init_string_id()-> ok.
