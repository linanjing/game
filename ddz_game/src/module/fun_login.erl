%% @author Administrator
%% @doc @todo Add description to fun_login.


-module(fun_login).
-include("common.hrl").
%% ====================================================================
%% API functions
%% ====================================================================
-export([login/1,login_check/1,join/1,
		 get_last_login_time/1,init/0,test/0,get_usr_name/1]).
               		 	
init()->
	?debug("fun_login init").
	
test()->
	R=db:dirty_match(us_club_member, #us_club_member{_='_',game_uid=100011,club_info_id=100001}),
	?debug("R=~p",[R]),
	ok.


join({{SceneId},Uid,Sid})->	
	case db:dirty_get(scene, SceneId) of
		[#scene{pid=Pid,type=_Type,club_id=ClubId}]->
			?debug("Uid=~p,ClubId=~p",[Uid,ClubId]),
			In=
				case db:dirty_match(us_club_member, #us_club_member{_='_',game_uid=Uid,club_info_id=ClubId}) of
					[#us_club_member{interger=Interger}]->?debug("1111111"),Interger;
					_->?debug("1111111"),0
				end,
			?debug("In=~p",[In]),
			case db:dirty_get(ply, Uid) of
				[#ply{coin=Coin,currency=Currency,name=Name,head_id=HeadId,
					  club_card_id=Club_card_id}]->
					CardInfo=fun_club:get_card_info(Club_card_id),	
					SceneCoin=#scene_coin{coin=Coin,currency=Currency,interger=In},
					gen_server:cast(Pid, {enter_scene,Uid,Sid,Name,HeadId,CardInfo,SceneCoin});
				_->skip
			end;	
		_->skip
	end.

%%agent_mng		
login({Data,Uid,Sid})->
	case db:dirty_get(ply, Uid) of
		[]->	
			Idx=erlang:get(agent_svr_index),
			case ets:lookup(agent_svr, Idx) of
				[#agent_svr{pid=Pid}]->				
					SceneIdList=fun_scene_select:get_scene_info(),
					gen_server:cast(Pid, {agent_add,Sid,SceneIdList,Data});
				_R->?debug("_R=~p",[_R]),skip
			end;
		_->skip
	end.

get_usr_name(Uid)->
	case db:dirty_get(ply, Uid) of
		[#ply{name=Name}]->Name;
		_->"游客"
	end.

login_check({Sid,{Accont,Machine_code,_PassWord,LoginType,Token,_Name},SceneIdList})->	
	case LoginType of 
		0->				
			case gen_server:call({global,http_svr}, {tourist_login,Machine_code},2000) of
				{ok,Body}->
					InfoList=jsx:decode(Body),
					{<<"data">>,AccData}=lists:keyfind(<<"data">>, 1, InfoList),			
					{<<"userGameId">>,ID}=lists:keyfind(<<"userGameId">>, 1, AccData),										
					{<<"userId">>,Aid}=lists:keyfind(<<"userId">>, 1, AccData),
					Uid=util:to_integer(ID),
					gen_server:cast(Sid, {set_val,agent_pid,self()}),
					gen_server:cast(Sid, {set_val,uid,Uid}),
					Currency=1000000,
					Coin=1000000,
					ClubInfoId=fun_club:get_club_info_id(Uid),
					erlang:put(uid, Uid),
					erlang:put(sid, Sid),
					erlang:put(name, tourist_name(Uid)),
					Usr=#ply{coin=Coin,sid=Sid,currency=Currency,uid=Uid,name=tourist_name(Uid),
							 device_no=Machine_code,aid=Aid,club_info_id=ClubInfoId},
					add_usr_online(Usr,Uid),				
					?debug("SceneIdList=~p",[SceneIdList]),
					pt:send(Sid, ?PT_RES_LOGIN, {Usr,Uid,
												 LoginType,
												 SceneIdList});
				_->skip
			end;

		_->
			case gen_server:call({global,http_svr}, {check_acc,Accont,Token},2000) of
				{ok,Body}->
					InfoList=jsx:decode(Body),
					{<<"data">>,RetData}=lists:keyfind(<<"data">>, 1, InfoList),
					{<<"amount">>,Currency}=lists:keyfind(<<"amount">>, 1, RetData),
					{<<"userGameId">>,ID}=lists:keyfind(<<"userGameId">>, 1, RetData),
					{<<"userId">>,Aid}=lists:keyfind(<<"userId">>, 1, RetData),
					Uid=util:to_integer(ID),
					gen_server:cast(Sid, {set_val,agent_pid,self()}),
					gen_server:cast(Sid, {set_val,uid,Uid}),
					Coin=1000000,
					ClubInfoId=fun_club:get_club_info_id(Uid),
					erlang:put(uid, Uid),  
					erlang:put(sid, Sid),
					erlang:put(name, tourist_name(Uid)),
					Usr=#ply{coin=Coin,sid=Sid,currency=1000000,uid=Uid,name=tourist_name(Uid),
							 device_no=Machine_code,aid=Aid,club_info_id=ClubInfoId},
					add_usr_online(Usr,Uid),			
					?debug("SceneIdList=~p",[SceneIdList]),
					pt:send(Sid, ?PT_RES_LOGIN, {Usr,Uid,
												 LoginType,
												 SceneIdList});
				_R->?debug("_R=~p",[_R]),skip
			end
	end.


get_last_login_time(_Uid)->0.

add_usr_online(Usr,Uid)->
	server_tools:send_to_agent_mng({usr_login_online,Usr,Uid}).
	
		
tourist_name(Uid)->
	"游客"++util:to_list(Uid).





		
			

