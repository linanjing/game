%% @author Administrator
%% @doc @todo Add description to fun_scene_open_card.


-module(fun_scene_open_card).

-include("common.hrl").
%% ====================================================================
%% 外部接口,处理所有下注的逻辑
%% ====================================================================
-define(Open_type1,1).
-define(Open_type2,2).
-define(Open_type3,3).
-define(Open_type4,4).
-define(Open_type5,5).
-export([start_game/0,sys_open_card/1,open_card/1,get_game_num/0,broad_cast/1]).

start_game()->
	 erlang:put(open_card_info, {0,[],5}),
	 update_game_num().

update_game_num()->
	Num=erlang:get(game_num),
	erlang:put(game_num, Num+1).

get_game_num()->
	Index=erlang:get(game_num),
	Index.

check_game_num(Num)->
	Index=erlang:get(game_num),
	Index==Num.

%% @doc 牛牛开牌
sys_open_card({Key,OpenType,LastGameNum})->
	case fun_scene_nn:get_status() of
		3->
			case check_game_num(LastGameNum) of
				true->
					case erlang:get(open_card_info) of
						{Uid,AllOpencardList,Key}->	
							{Key,CardList,Bets}=lists:keyfind(Key, 1, AllOpencardList),	
							F=fun(Card=#card{})->Card#card{open=1} end,
							NewCardList=lists:map(F, CardList),
							NewOpenCardList=lists:keyreplace(Key, 1, AllOpencardList, {Key,NewCardList,Bets}),
							broad_cast({?PT_RES_NIUNIU_CARD_INFO, {Uid,Key,NewCardList,OpenType}}),	
							NewKey=Key-1,
							GameNum=get_game_num(),
							JiangeTime=fun_scene_time:get_kapaijiange_time(),
							TexiaoTime=fun_scene_time:get_kaipaitexiao_time(),
							if
								NewKey>0->
									{NewKey,_NewL,NextBets}= lists:keyfind(NewKey, 1, AllOpencardList) ,
									?debug("NewKey=~p~n",[NewKey]),
									?debug("NextBets=~p",[NextBets]),
									NewUid=get_next_uid(NextBets),
									erlang:put(open_card_info, {NewUid,NewOpenCardList,NewKey}),			
									?debug("Name=~p",["小薇创想"]),
									?debug("Name=~p",[get_name(NewUid)]),
									Msg={?PT_NOTYFY_NEXT_OPEN_CARD, {NewUid,NewKey,get_name(NewUid)}},							
									timer:apply_after(TexiaoTime, gen_server, cast, [self(), {process_pt,?MODULE,
																						broad_cast,
																						Msg}]),
									
									if
										NewUid==0->
											?debug("NewUid=~p",[NewUid]),
											timer:apply_after(JiangeTime, gen_server, cast, [self(), 
																					   {process_pt,?MODULE,
																						sys_open_card,
																						{NewKey,?Open_type5,GameNum}}]);
										true->										
											#player{ai=AI}=erlang:get(NewUid),
											case AI of
												true->
													RandTime=util:rand(1, 8),
													NextOpenTimne=RandTime*1000+JiangeTime,
													timer:apply_after(NextOpenTimne, gen_server, cast, [self(),
																										{process_pt,?MODULE,
																										 sys_open_card,
																										 {NewKey,?Open_type5,GameNum}}]);
												_->
													OpenCardTime=fun_scene_time:get_open_card_time(),
													timer:apply_after(OpenCardTime, gen_server, cast, [self(), 
																								{process_pt,?MODULE,
																								 sys_open_card,
																								 {NewKey,?Open_type5,GameNum}}])
											end
									end;
								true->
										timer:apply_after(JiangeTime, gen_server, cast, [self(), 
																								{process_pt,fun_scene_nn,
																								 game_over
																								 }])
							end;
						_->skip
					end;
				_->skip
			end;
		_->skip
	end.
check_all_open(List)->
	F=fun(#card{open=Open})->
			  Open==1 end,
	lists:all(F, List).
open_card({Key,0,OpenType})->
	case erlang:get(open_card_info) of
		{Uid,OpenCardInfoList,Key}->
			{Key,CardList,Bets}=lists:keyfind(Key, 1, OpenCardInfoList),
			case check_all_open(CardList) of
				false->
			NewKey=Key-1,		
			F=fun(Card=#card{open=Open},{CardL1,CardL2})->
					  if
						  Open==0 -> {CardL1++[Card#card{open=1}],CardL2++[Card#card{open=1}]};
						  true->{CardL1++[Card],CardL2}
					  end end,
			{NewCardList,Need}=lists:foldl(F, {[],[]}, CardList),
			NewOpenCardInfoList=lists:keyreplace(Key, 1, OpenCardInfoList, {Key,NewCardList,Bets}),	
			
			broad_cast({?PT_RES_NIUNIU_CARD_INFO, {Uid,Key,Need,OpenType}}),	
			Game_num=get_game_num(),
			JiangeTime=fun_scene_time:get_kapaijiange_time(),
			TexiaoTime=fun_scene_time:get_kaipaitexiao_time(),
			case check_open_all(NewCardList) of
				true->
					if
						NewKey>=1->	
							{NewKey,_,NextBets}=lists:keyfind(NewKey, 1, NewOpenCardInfoList),
						NewUid=get_next_uid(NextBets),
%% 									?debug("sys_open_card NewUid=~p,NextBets=~p",[NewUid,NextBets]),
%% 							broad_cast(?PT_NOTYFY_NEXT_OPEN_CARD, {NewUid,NewKey,get_name(Uid)}),
							Msg={?PT_NOTYFY_NEXT_OPEN_CARD, {NewUid,NewKey,get_name(Uid)}},
									timer:apply_after(JiangeTime, gen_server, cast, [self(), {process_pt,?MODULE,
																						broad_cast,
																						Msg}]),
								if
								NewUid==0->
									timer:apply_after(TexiaoTime, gen_server, cast, [self(), {process_pt,?MODULE,
																						sys_open_card,
																						{NewKey,?Open_type5,Game_num}}]);
								true->
									#player{ai=AI}=erlang:get(Uid),
									case AI of
										true->
											RandTime=util:rand(1, 8),
											NextOpenTimne=RandTime*1000+TexiaoTime,
											timer:apply_after(NextOpenTimne, gen_server, cast, [self(),
																						 {process_pt,?MODULE,
																						  sys_open_card,
																						  {NewKey,?Open_type5,Game_num}}]);
										_->
											OpenCardTime=fun_scene_time:get_open_card_time(),
									      timer:apply_after(OpenCardTime, gen_server, cast, [self(), 
																				{process_pt,?MODULE,
																				 sys_open_card,{NewKey,?Open_type5,Game_num}}])
									end
							end,
							erlang:put(open_card_info, {NewUid,NewOpenCardInfoList,NewKey});
						true->timer:apply_after(JiangeTime, gen_server, cast, [self(), 
																				{process_pt,fun_scene_nn,
																				 game_over}])
					end;		
				_->
					erlang:put(open_card_info, {Uid,NewOpenCardInfoList,Key})
			end;
				_->skip
			end;
		_->skip
	end;
open_card({Key,Index,OpenType})->
	case erlang:get(open_card_info) of
		{Uid,OpenCardInfoList,Key}->
			{Key,CardList,Bets}=lists:keyfind(Key, 1, OpenCardInfoList),
			NewKey=Key-1,
			case lists:keyfind(Index, #card.index, CardList) of
				Card=#card{index=Index,open=0}->
					NewCard=	 Card#card{open=1},
					NewCardList=lists:keyreplace(Index, #card.index, CardList, NewCard),
					NewOpenCardInfoList=lists:keyreplace(Key, 1, OpenCardInfoList, {Key,NewCardList,Bets}),	
					broad_cast({?PT_RES_NIUNIU_CARD_INFO, {Uid,Key,[NewCard],OpenType}}),	
					Game_num=get_game_num(),
						JiangeTime=fun_scene_time:get_kapaijiange_time(),
			TexiaoTime=fun_scene_time:get_kaipaitexiao_time(),
					case check_open_all(NewCardList) of
						true->
							if
								NewKey>=1->	
									{NewKey,_,NextBets}=lists:keyfind(NewKey, 1, NewOpenCardInfoList),
									NewUid=get_next_uid(NextBets),
%% 										?debug("sys_open_card NewUid=~p,NextBets=~p",[NewUid,NextBets]),
									?debug("Name=~p",["小薇创想"]),
									?debug("Name=~p",[get_name(Uid)]),
									Msg={?PT_NOTYFY_NEXT_OPEN_CARD, {NewUid,NewKey,get_name(Uid)}},
									timer:apply_after(JiangeTime, gen_server, cast, [self(), {process_pt,?MODULE,
																						broad_cast,
																						Msg}]),
										if
								NewUid==0->
									timer:apply_after(TexiaoTime, gen_server, cast, [self(), {process_pt,?MODULE,
																						sys_open_card,
																						{NewKey,?Open_type5,Game_num}}]);
								true->
									#player{ai=AI}=erlang:get(Uid),
									case AI of
										true->
											RandTime=util:rand(1, 8),
											NextOpenTimne=RandTime*1000+TexiaoTime,
											timer:apply_after(NextOpenTimne, gen_server, cast, [self(),
																						 {process_pt,?MODULE,
																						  sys_open_card,
																						  {NewKey,?Open_type5,Game_num}}]);
										_->
											OpenCardTime=fun_scene_time:get_open_card_time(),
									timer:apply_after(OpenCardTime, gen_server, cast, [self(), 
																				{process_pt,?MODULE,
																				 sys_open_card,{NewKey,?Open_type5,Game_num}}])
									end
							end,
									erlang:put(open_card_info, {NewUid,NewOpenCardInfoList,NewKey});
								true->
										timer:apply_after(JiangeTime, gen_server, cast, [self(), 
																				{process_pt,fun_scene_nn,
																				 game_over}])
	
							end;		
						_->
							erlang:put(open_card_info, {Uid,NewOpenCardInfoList,Key})
					end;
				_->skip
			end;
		_->skip
	end.
	
get_next_uid([])->0;
get_next_uid([{Uid,Bet}|Next])->
	if
		Bet>0->Uid;
		true->get_next_uid(Next)
	end.

broad_cast({Cmd,Data})->
	?debug("Cmd=~p,Data=~p",[Cmd,Data]),
	UsrList=erlang:get(usr),
	F=fun(Uid)->
			case erlang:get(Uid) of
				#player{sid=Sid,ai=false,leave=0}->
				pt:send(Sid,Cmd,Data);
				_->skip 
			end
	  	end,
	lists:foreach(F, UsrList).

						 
get_name(0)-> erlang:get(masterName);
get_name(Uid)->
%% 	?debug("machine get_name"),
	Play=erlang:get(Uid),
	Play#player.name.

	
check_open_all(List)->
	F=fun(#card{open=Open})->
			  	Open==1 end,
	lists:all(F, List).
						 