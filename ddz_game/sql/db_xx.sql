﻿# Host: localhost  (Version: 5.6.40-log)
# Date: 2018-11-29 20:39:09
# Generator: MySQL-Front 5.3  (Build 2.42)

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE */;
/*!40101 SET SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES */;
/*!40103 SET SQL_NOTES='ON' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

#
# Source for table "account"
#

DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `id` int(11) unsigned NOT NULL DEFAULT '0',
  `token_id` varchar(64) NOT NULL,
  `head_url` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `regist_time` int(11) unsigned NOT NULL DEFAULT '0',
  `last_login_time` int(11) unsigned NOT NULL DEFAULT '0',
  `skin_id` int(11) unsigned NOT NULL DEFAULT '0',
  `diamond` bigint(20) unsigned NOT NULL DEFAULT '0',
  `sex` int(11) unsigned NOT NULL,
  `head_frame` int(11) unsigned NOT NULL,
  `bullet_shin` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token_id` (`token_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "account"
#

INSERT INTO `account` VALUES (1,'linanjing','https://fq-1256666226.cos.ap-guangzhou.myqcloud.com/userAvarta6.png','linanjing_name',1543494255,1543494255,10001,10,2,400,0),(2,'1999','https://fq-1256666226.cos.ap-guangzhou.myqcloud.com/userAvarta9.png','1999_name',1543494268,1543494268,10001,10,2,400,0);

#
# Source for table "usr_achieve_info"
#

DROP TABLE IF EXISTS `usr_achieve_info`;
CREATE TABLE `usr_achieve_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(64) NOT NULL,
  `reward_list` varchar(64) NOT NULL,
  `achieve_list` varchar(255) NOT NULL,
  `point` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token_id` (`token_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

#
# Data for table "usr_achieve_info"
#

INSERT INTO `usr_achieve_info` VALUES (1,'linanjing','[]','[{1,1,1543494255}]',0),(2,'1999','[]','[{1,1,1543494268}]',0);

#
# Source for table "usr_bag_info"
#

DROP TABLE IF EXISTS `usr_bag_info`;
CREATE TABLE `usr_bag_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(64) NOT NULL,
  `bag_list` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `token_id` (`token_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "usr_bag_info"
#


#
# Source for table "usr_battle_info"
#

DROP TABLE IF EXISTS `usr_battle_info`;
CREATE TABLE `usr_battle_info` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(64) NOT NULL DEFAULT '',
  `name` varchar(64) NOT NULL DEFAULT '',
  `headurl` varchar(64) NOT NULL DEFAULT '',
  `game_num` int(11) unsigned NOT NULL DEFAULT '0',
  `kill_num` int(11) unsigned NOT NULL DEFAULT '0',
  `assist_num` int(11) unsigned NOT NULL DEFAULT '0',
  `dead_num` int(11) unsigned NOT NULL DEFAULT '0',
  `victory_num` int(11) unsigned NOT NULL DEFAULT '0',
  `fail_num` int(11) unsigned NOT NULL DEFAULT '0',
  `max_continuous_kill_num` int(11) unsigned NOT NULL DEFAULT '0',
  `mvp_num` int(11) unsigned NOT NULL DEFAULT '0',
  `points` int(11) unsigned NOT NULL DEFAULT '0',
  `star` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `token_id` (`token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "usr_battle_info"
#


#
# Source for table "usr_endless_mode_info"
#

DROP TABLE IF EXISTS `usr_endless_mode_info`;
CREATE TABLE `usr_endless_mode_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `points` int(11) unsigned NOT NULL,
  `kill_count` int(11) unsigned NOT NULL,
  `start_time` int(11) unsigned NOT NULL,
  `end_time` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token_id` (`token_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

#
# Data for table "usr_endless_mode_info"
#

INSERT INTO `usr_endless_mode_info` VALUES (1,'linanjing','linanjing_name',0,0,0,0),(2,'1999','1999_name',0,0,0,0);

#
# Source for table "usr_gift_info"
#

DROP TABLE IF EXISTS `usr_gift_info`;
CREATE TABLE `usr_gift_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(64) NOT NULL,
  `reward_list` varchar(64) NOT NULL,
  `person_list` text NOT NULL,
  `novice_list` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token_id` (`token_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

#
# Data for table "usr_gift_info"
#

INSERT INTO `usr_gift_info` VALUES (1,'9999','[]','[]','[\"1999\",\"linanjing\"]'),(2,'linanjing','[]','[]','[]'),(3,'1999','[]','[]','[]');

#
# Source for table "usr_shop_info"
#

DROP TABLE IF EXISTS `usr_shop_info`;
CREATE TABLE `usr_shop_info` (
  `id` int(11) unsigned NOT NULL,
  `token_id` varchar(64) NOT NULL,
  `shop_list` varchar(255) NOT NULL DEFAULT '[]',
  PRIMARY KEY (`id`),
  UNIQUE KEY `token_id` (`token_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "usr_shop_info"
#


#
# Source for table "usr_sign_info"
#

DROP TABLE IF EXISTS `usr_sign_info`;
CREATE TABLE `usr_sign_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `token_id` varchar(64) NOT NULL,
  `sign_list` varchar(64) NOT NULL DEFAULT '[]',
  `is_sign` int(11) unsigned NOT NULL DEFAULT '0',
  `sign_day` int(11) unsigned NOT NULL DEFAULT '0',
  `sign_time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `token_id` (`token_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "usr_sign_info"
#


#
# Source for table "world_rank"
#

DROP TABLE IF EXISTS `world_rank`;
CREATE TABLE `world_rank` (
  `token_id` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `sex` int(11) NOT NULL,
  `head_url` varchar(255) NOT NULL,
  `head_frame` int(11) NOT NULL,
  `star` int(11) unsigned NOT NULL DEFAULT '0',
  `type` int(11) unsigned NOT NULL,
  PRIMARY KEY (`token_id`),
  UNIQUE KEY `token_id` (`token_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "world_rank"
#


/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
