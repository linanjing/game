﻿# Host: localhost  (Version 5.6.40-log)
# Date: 2019-03-25 11:38:40
# Generator: MySQL-Front 6.1  (Build 1.26)


#
# Structure for table "club"
#

DROP TABLE IF EXISTS `club`;
CREATE TABLE `club` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `player_num` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `create_player_id` int(11) NOT NULL DEFAULT '0',
  `club_info_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `club_info` (`club_info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

#
# Structure for table "club_member_info"
#

DROP TABLE IF EXISTS `club_member_info`;
CREATE TABLE `club_member_info` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0',
  `club_id` int(11) NOT NULL DEFAULT '0',
  `req_state` int(11) NOT NULL DEFAULT '0',
  `enter_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

#
# Structure for table "usr"
#

DROP TABLE IF EXISTS `usr`;
CREATE TABLE `usr` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `head_img` varchar(255) NOT NULL DEFAULT '',
  `session_key` varchar(255) NOT NULL DEFAULT '',
  `sex` int(11) NOT NULL DEFAULT '0',
  `channel_id` int(11) NOT NULL DEFAULT '0',
  `gold` int(11) NOT NULL DEFAULT '0',
  `diamond` int(11) NOT NULL DEFAULT '0',
  `machine_code` varchar(255) NOT NULL DEFAULT '',
  `head_id` int(11) NOT NULL DEFAULT '0',
  `coin` int(11) NOT NULL DEFAULT '0',
  `currency` int(11) NOT NULL DEFAULT '0',
  `last_login_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `usr` (`machine_code`),
  KEY `session_key` (`session_key`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
