/*
Navicat MySQL Data Transfer

Source Server         : game_db
Source Server Version : 50562
Source Host           : localhost:3306
Source Database       : db_xx

Target Server Type    : MYSQL
Target Server Version : 50562
File Encoding         : 65001

Date: 2018-11-23 18:58:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for account
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `id` int(11) unsigned NOT NULL DEFAULT '0',
  `token_id` varchar(64) NOT NULL,
  `head_url` varchar(225) NOT NULL,
  `name` varchar(64) NOT NULL,
  `regist_time` int(11) unsigned NOT NULL DEFAULT '0',
  `last_login_time` int(11) unsigned NOT NULL DEFAULT '0',
  `skin_id` int(11) unsigned NOT NULL DEFAULT '0',
  `diamond` bigint(20) unsigned NOT NULL DEFAULT '0',
  `sex` int(11) unsigned NOT NULL,
  `head_frame` int(11) unsigned NOT NULL,
  `bullet_shin` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token_id` (`token_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for usr_achieve_info
-- ----------------------------
DROP TABLE IF EXISTS `usr_achieve_info`;
CREATE TABLE `usr_achieve_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(64) NOT NULL,
  `reward_list` varchar(64) NOT NULL,
  `achieve_list` varchar(255) NOT NULL,
  `point` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token_id` (`token_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for usr_bag_info
-- ----------------------------
DROP TABLE IF EXISTS `usr_bag_info`;
CREATE TABLE `usr_bag_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(64) NOT NULL,
  `bag_list` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `token_id` (`token_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for usr_battle_info
-- ----------------------------
DROP TABLE IF EXISTS `usr_battle_info`;
CREATE TABLE `usr_battle_info` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(64) NOT NULL DEFAULT '',
  `name` varchar(64) NOT NULL DEFAULT '',
  `headurl` varchar(64) NOT NULL DEFAULT '',
  `game_num` int(11) unsigned NOT NULL DEFAULT '0',
  `kill_num` int(11) unsigned NOT NULL DEFAULT '0',
  `assist_num` int(11) unsigned NOT NULL DEFAULT '0',
  `dead_num` int(11) unsigned NOT NULL DEFAULT '0',
  `victory_num` int(11) unsigned NOT NULL DEFAULT '0',
  `fail_num` int(11) unsigned NOT NULL DEFAULT '0',
  `max_continuous_kill_num` int(11) unsigned NOT NULL DEFAULT '0',
  `mvp_num` int(11) unsigned NOT NULL DEFAULT '0',
  `points` int(11) unsigned NOT NULL DEFAULT '0',
  `star` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `token_id` (`token_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for usr_endless_mode_info
-- ----------------------------
DROP TABLE IF EXISTS `usr_endless_mode_info`;
CREATE TABLE `usr_endless_mode_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `points` int(11) unsigned NOT NULL,
  `kill_count` int(11) unsigned NOT NULL,
  `start_time` int(11) unsigned NOT NULL,
  `end_time` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token_id` (`token_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for usr_gift_info
-- ----------------------------
DROP TABLE IF EXISTS `usr_gift_info`;
CREATE TABLE `usr_gift_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token_id` varchar(64) NOT NULL,
  `reward_list` varchar(64) NOT NULL,
  `person_list` text NOT NULL,
  `novice_list` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token_id` (`token_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for usr_shop_info
-- ----------------------------
DROP TABLE IF EXISTS `usr_shop_info`;
CREATE TABLE `usr_shop_info` (
  `id` int(11) unsigned NOT NULL,
  `token_id` varchar(64) NOT NULL,
  `shop_list` varchar(255) NOT NULL DEFAULT '[]',
  PRIMARY KEY (`id`),
  UNIQUE KEY `token_id` (`token_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for usr_sign_info
-- ----------------------------
DROP TABLE IF EXISTS `usr_sign_info`;
CREATE TABLE `usr_sign_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `token_id` varchar(64) NOT NULL,
  `sign_list` varchar(64) NOT NULL DEFAULT '[]',
  `is_sign` int(11) unsigned NOT NULL DEFAULT '0',
  `sign_day` int(11) unsigned NOT NULL DEFAULT '0',
  `sign_time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `token_id` (`token_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for world_rank
-- ----------------------------
DROP TABLE IF EXISTS `world_rank`;
CREATE TABLE `world_rank` (
  `token_id` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `sex` int(11) NOT NULL,
  `head_url` varchar(255) NOT NULL,
  `head_frame` int(11) NOT NULL,
  `star` int(11) unsigned NOT NULL DEFAULT '0',
  `type` int(11) unsigned NOT NULL,
  PRIMARY KEY (`token_id`),
  UNIQUE KEY `token_id` (`token_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
